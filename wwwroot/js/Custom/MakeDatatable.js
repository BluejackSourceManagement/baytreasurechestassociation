﻿function makeInvisibleDatatable(tableDiv, columnDefs, exportOptions, sortedColumns) {
    $('#' + tableDiv).on('init.dt', function (e, settings, json) {
        let rowHtml = '<div class="row" id="tableControlRow">' +
            '<div class="col-4" id="exportButtons">' +
            '</div>';
        $('#' + tableDiv + '_wrapper').prepend(rowHtml);
        $('#exportButtons').append($('.dt-buttons'));
        // this is a pure hack to compensate for the
        // export buttons destroying the page length
        // dropdown selector
        $('#rowCountSelector').append(settings.oApi._fnFeatureHtmlLength(settings));
        $('#searchBox').append($('#' + tableDiv + '_filter'));
        clearForm();
    }).dataTable({
        "deferRender": true,
        "columnDefs": columnDefs,
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'copyHtml5',
                exportOptions: exportOptions
            },
            {
                extend: 'excelHtml5',
                exportOptions: exportOptions
            },
            {
                extend: 'csvHtml5',
                exportOptions: exportOptions
            }
        ],
        "order": sortedColumns === undefined ? [] : sortedColumns,
        "pageLength": 100
    });
}
function makeDatatable(tableDiv, columnDefs, exportOptions, sortedColumns) {
    $('#' + tableDiv).on('init.dt', function (e, settings, json) {
        let rowHtml = '<div class="row" id="tableControlRow">' +
            '<div class="col-4" id="exportButtons">' +
            '</div>' +
            '<div class="col-4" id="rowCountSelector" style="text-align:center">' +
            '</div>' +
            '<div class="col-4" id="searchBox">' +
            '</div>' +
            '</div>';
        $('#' + tableDiv + '_wrapper').prepend(rowHtml);
        $('#exportButtons').append($('.dt-buttons'));
        // this is a pure hack to compensate for the
        // export buttons destroying the page length
        // dropdown selector
        $('#rowCountSelector').append(settings.oApi._fnFeatureHtmlLength(settings));
        $('#searchBox').append($('#' + tableDiv + '_filter'));
        //clearForm();
    }).dataTable({
        "deferRender":true,
        "columnDefs": columnDefs,
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'copyHtml5',
                exportOptions: exportOptions
            },
            {
                extend: 'excelHtml5',
                exportOptions: exportOptions
            },
            {
                extend: 'csvHtml5',
                exportOptions: exportOptions
            }
        ],
        "order": sortedColumns === undefined ? [] : sortedColumns,
        "pageLength": 100
    });
}
function makeDatatable2(tableDiv, columnDefs, exportOptions, sortedColumns) {
    $('#' + tableDiv).on('init.dt', function (e, settings, json) {
        let rowHtml = '<div class="row" id="tableControlRow2">' +
            '<div class="col-4" id="exportButtons2">' +
            '</div>' +
            '<div class="col-4" id="rowCountSelector2" style="text-align:center">' +
            '</div>' +
            '<div class="col-4" id="searchBox2">' +
            '</div>' +
            '</div>';
        $('#' + tableDiv + '_wrapper').prepend(rowHtml);
        let newButtons = $('.dt-buttons')[1];
        $('#exportButtons2').append(newButtons);
        // this is a pure hack to compensate for the
        // export buttons destroying the page length
        // dropdown selector
        $('#rowCountSelector2').append(settings.oApi._fnFeatureHtmlLength(settings));
        $('#searchBox2').append($('#' + tableDiv + '_filter'));
        //clearForm();
    }).dataTable({
        "deferRender": true,
        "columnDefs": columnDefs,
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'copyHtml5',
                exportOptions: exportOptions
            },
            {
                extend: 'excelHtml5',
                exportOptions: exportOptions
            },
            {
                extend: 'csvHtml5',
                exportOptions: exportOptions
            }
        ],
        "order": sortedColumns === undefined ? [] : sortedColumns,
        "pageLength": 100
    });
}
function makeNonExportingDatatable(tableDiv, columnDefs, sortedColumns) {
    $('#' + tableDiv).on('init.dt', function (e, settings, json) {
    }).dataTable({
        "deferRender": true,
        "columnDefs": columnDefs,
        "order": sortedColumns === undefined ? [] : sortedColumns,
        "pageLength": 100
    });
}

function makeTenRowNonExportingDatatable(tableDiv, columnDefs, sortedColumns) {
    $('#' + tableDiv).on('init.dt', function (e, settings, json) {
    }).dataTable({
        "deferRender": true,
        "columnDefs": columnDefs,
        "order": sortedColumns === undefined ? [] : sortedColumns
    });
}

function makeTenRowDatatable(tableDiv, columnDefs, exportOptions, sortedColumns) {
    $('#' + tableDiv).on('init.dt', function (e, settings, json) {
        let rowHtml = '<div class="row" id="tableControlRow">' +
            '<div class="col-4" id="exportButtons">' +
            '</div>' +
            '<div class="col-4" id="rowCountSelector" style="text-align:center">' +
            '</div>' +
            '<div class="col-4" id="searchBox">' +
            '</div>' +
            '</div>';
        $('#' + tableDiv + '_wrapper').prepend(rowHtml);
        $('#exportButtons').append($('.dt-buttons'));
        // this is a pure hack to compensate for the
        // export buttons destroying the page length
        // dropdown selector
        $('#rowCountSelector').append(settings.oApi._fnFeatureHtmlLength(settings));
        $('#searchBox').append($('#' + tableDiv + '_filter'));
        //clearForm();
    }).dataTable({
        "deferRender": true,
        "columnDefs": columnDefs,
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'copyHtml5',
                exportOptions: exportOptions
            },
            {
                extend: 'excelHtml5',
                exportOptions: exportOptions
            },
            {
                extend: 'csvHtml5',
                exportOptions: exportOptions
            }
        ],
        "order": sortedColumns === undefined ? [] : sortedColumns
    });
}