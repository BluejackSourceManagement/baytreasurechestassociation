﻿$(document).ready(
    function () {
        $('.bj-other-combo').on('change', function (evt) {
            let optionSelected = $("option:selected", this);
            let valueSelected = this.value;
            if (valueSelected === 'Other (See or Add Info in Comments)') {
                $('.bj-phone-number').val("Other (See or Add Info in Comments)").prop("disabled", true);
                $('.bj-postal-code').val("Other (See or Add Info in Comments)").prop("disabled", true);
                $('.bj-communities > option').each(function (idx, option) {
                    if (option.value === "Other (See or Add Info in Comments)") {
                        $(option).prop({
                            "selected": true
                        });
                        $('.bj-communities').prop("disabled", true);
                    }
                });
                $('.bj-provinces > option').each(function (idx, option) {
                    if (option.value === "Other (See or Add Info in Comments)") {
                        $(option).prop({
                            "selected": true
                        });
                        $('.bj-provinces').prop("disabled", true);
                    }
                });
            }
            else {
                $('.bj-phone-number').val("").prop({
                    "disabled": false,
                    "placeholder": "XXX-XXX-XXXX"
                });
                $('.bj-postal-code').val("").prop({
                    "disabled": false,
                    "placeholder": "Example: A1A 1A1"
                });

                $('.bj-communities').prop("disabled", false);
                $('.bj-phone-number').prop("disabled", false);
                $('.bj-provinces').prop("disabled", false);

            }
        });
    }
);