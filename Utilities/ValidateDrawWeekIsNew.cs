﻿using BayTreasureChestAssociation.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BayTreasureChestAssociation.Utilities
{
    public class ValidateDrawWeekIsNew : ValidationAttribute
    {
        private readonly ApplicationDbContext _db;
        public ValidateDrawWeekIsNew(ApplicationDbContext db)
        {
            _db = db;
        }
        public override bool IsValid(object value)
        {
            return base.IsValid(value);
        }
    }
}
