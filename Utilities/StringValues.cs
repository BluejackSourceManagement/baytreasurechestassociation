﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BayTreasureChestAssociation.Utilities
{
    public static class StringValues
    {
        public const string AppAdministrator = "Administrator";
        public const string AppEditor = "Editor";
        public const string ReadOnlyUser = "ReadOnly";

    }
}
