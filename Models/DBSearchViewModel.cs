﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BayTreasureChestAssociation.Models
{
    public class DBSearchViewModel
    {
        public string RegistrationNumber { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Province { get; set; }
        public string PostalCode { get; set; }
        public string Community { get; set; }
        public string PlayerComments { get; set; }
        public string RegistrationComments { get; set; }
        public string DateRegistered { get; set; }
        public string DateRegisteredEnd { get; set; }
        public string DateRetired { get; set; }
        public string DateRetiredEnd { get; set; }
        public string Country { get; set; }
        public string Status { get; set; }
        public string RequiresContact { get; set; }
        public string LastUpdated { get; set; }
        public string LastUpdatedEnd { get; set; }
    }
}
