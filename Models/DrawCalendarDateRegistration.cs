﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BayTreasureChestAssociation.Models
{
    public class DrawCalendarDate
    {
        public CalendarDate CalendarDate { get; set; }
        public Draw Draw { get; set;}
    }
}
