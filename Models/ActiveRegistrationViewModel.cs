﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BayTreasureChestAssociation.Models
{
    public class ActiveRegistrationViewModel
    {
        public int Total { get; set; }
        public int TotalPlayed { get; set; }
        public string Week { get; set; }
        public int WeekNumber { get; set; }
        public decimal Percentage { get { return Math.Round(((Convert.ToDecimal(TotalPlayed) / Convert.ToDecimal(Total)) * 100), 2); } }
        public SearchType SearchType { get; set; }
    }
}
