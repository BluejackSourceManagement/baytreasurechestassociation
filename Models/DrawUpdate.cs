﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BayTreasureChestAssociation.Models
{
    public class DrawUpdate
    {
        public int DrawId { get; set; }
        public int StickerCount { get; set; }
        public string Comments { get; set; }
    }
}
