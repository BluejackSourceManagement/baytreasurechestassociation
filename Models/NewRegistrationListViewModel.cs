﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BayTreasureChestAssociation.Models
{
    public class NewRegistrationListViewModel
    {
        public List<PlayerRegistration> PlayerRegistrations { get; set; }
        public DateTime RegistrationDate { get; set; }
        public NewRegistrationListViewModel()
        {
            PlayerRegistrations = new List<PlayerRegistration>();
        }
        public SearchType SearchType { get; set; }
    }
    public enum SearchType
    {
        BothNull = 0,
        StartDateOnly,
        EndDateOnly,
        RangeDefined
    }
    public class PlayerRegistration
    {
        public Player Player { get; set; }
        public Registration Registration { get; set; }
        public SearchType SearchType { get; set; }
    }
}
