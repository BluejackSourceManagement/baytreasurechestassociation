﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BayTreasureChestAssociation.Models
{
    public class WinnerLog
    {
        [Required]
        [Display(Name = "Id")]
        public int Id { get; set; }

        /// <summary>
        /// Was the number that was drawn actually played by a human?
        /// </summary>
        [Display(Name = "Was Played?")]
        public bool WasPlayed { get; set; }

        [Display(Name = "Bank Deposit")]
        [Required]
        [DataType(DataType.Currency)]
        public decimal BankDeposit { get; set; }

        [Display(Name = "Possible Prize")]
        [DataType(DataType.Currency)]
        public decimal PossiblePrize { get; set; }

        [Display(Name = "Actual Prize")]
        [DataType(DataType.Currency)]
        public decimal ActualPrize { get; set; }

        [Display(Name = "Location Played")]
        public string LocationPlayed { get; set; }

        [Display(Name = "Rollover?")]
        public bool HasRollover { get; set; }

        [Display(Name = "Rollover Amount")]
        [DataType(DataType.Currency)]
        public decimal RolloverAmount { get; set; }

        [Display(Name = "Rollover Count")]
        public int RolloverCount { get; set; }

        [Display(Name = "Cheque Number")]
        public string ChequeNumber { get; set; }

        [Display(Name = "Comments")]
        public string Comments { get; set; }

        //relationships


        /// <summary>
        /// A winner log is associated with a Draw, from draw you can get the registration and then the player info
        /// </summary>
        [Display(Name = "Draw ID")]
        public int? DrawID { get; set; }

        [Display(Name = "Draw ID")]
        [ForeignKey("DrawID")]
        public virtual Draw Draw { get; set; }

    }
}
