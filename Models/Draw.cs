﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BayTreasureChestAssociation.Models
{
    public class Draw
    {
        [Required]
        [Display(Name = "Id")]
        public int Id { get; set; }

        /// <summary>
        /// for this particular draw was there a winner?
        /// </summary>
        [Display(Name = "Winner Drawn?")]
        public bool HasWinner { get; set; }

        [Display(Name = "Comments")]
        [DataType(DataType.MultilineText)]
        public string Comments { get; set; }

        [Display(Name = "Sticker Count")]
        public int StickerCount { get; set; }

        [Display(Name = "Question Sticker Count")]
        public int UnknownStickerCount { get; set; }

        [Display(Name = "Total Sticker Count")]
        public int TotalStickerCount { get; set; }

        [Display(Name = "Expected Sticker Count")]
        public int ExpectedStickerCount { get; set; }

        [Display(Name = "Under-Stickered Dollar Bills($)")]
        [DataType(DataType.Currency)]
        public decimal UnderStickeredDollarBills { get; set; }

        [Display(Name = "Bare Coins($)")]
        [DataType(DataType.Currency)]
        public decimal BareCoins { get; set; }

        [Display(Name = "Disqualified Pink Sheet Stickers($)")]
        [DataType(DataType.Currency)]
        public decimal DisqualifiedPinkSheetStickers { get; set; }

        [Display(Name = "Known Variance($)")]
        [DataType(DataType.Currency)]
        public decimal KnownVariance { get; set; }

        [Display(Name = "New Registrations")]
        public int NewRegistrations { get; set; }

        [Display(Name = "Supervisor")]
        public string Supervisor { get; set; }

        //Relationships

        /// <summary>
        /// A draw is associated with a registration ID that won the draw 
        /// and can trace back to player information, one to one relations - nullable ID to prevent circular ref errors
        /// </summary>
        [Display(Name = "Winner Registration Id")]
        public int? RegistrationID { get; set; }

        [Display(Name = "Winner Registration Number")]
        [ForeignKey("RegistrationID")]
        public virtual Registration Registration { get; set; }


        /// <summary>
        /// a Draw has a calendar date, one to one relationshp - nullable ID to prevent circular ref errors
        /// </summary>
        [Display(Name = "Draw Date")]
        public int? CalendarDateID { get; set; }

        [Display(Name = "Draw Date")]
        [ForeignKey("CalendarDateID")]
        public virtual CalendarDate CalendarDate { get; set; }
    }
}
