﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BayTreasureChestAssociation.Models
{
    public class AllRegistrationsViewModel
    {
        public string Week { get; set; }
        public int TotalActive { get; set; }
        public int TotalPlayed { get; set; }
        public int NewRegistrations { get; set; }
        public int RetiredRegistrations { get; set; }
        public SearchType SearchType { get; set; }
    }
}
