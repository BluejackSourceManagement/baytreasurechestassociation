﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BayTreasureChestAssociation.Models
{
    public class Player
    {
        [Required]
        [Display(Name = "Id")]
        public int Id { get; set; }

        [Required]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Display(Name = "Middle Name")]
        public string MiddleName { get; set; }

        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Display(Name = "Address")]
        public string Address { get; set; }

        [Display(Name = "Country")]
        public string Country { get; set; }

        [Display(Name = "Community")]
        public string Community { get; set; }

        [Display(Name = "Province")]
        public string Province { get; set; }

        [Display(Name = "Postal Code")]
        [DataType(DataType.PostalCode)]
        public string Postal { get; set; }

        [Display(Name = "Email")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Display(Name = "Phone")]
        [DataType(DataType.PhoneNumber)]
        public string Phone { get; set; }

        [Display(Name = "Player Comments")]
        [DataType(DataType.MultilineText)]
        public string Comments { get; set; }

        [Display(Name = "Last Updated")]
        [DataType(DataType.DateTime)]
        public DateTime LastUpdated { get; set; }

        [Display(Name = "Requires Contact")]
        public bool RequiresContact { get; set; }

        /// <summary>
        /// This is to support Dashboard Reporting Requirements
        /// </summary>
        [Display(Name = "Missing Info?")]
        public bool HasMissingInfo { get { return string.IsNullOrEmpty(Email) && string.IsNullOrEmpty(Phone); } }

        /// <summary>
        /// A player can own multiple registrations
        /// </summary>
        public IList<Registration> Registrations { get; set; }

        public bool Equals(Player player)
        {
            bool postalIsSame = false;
            if (Postal == null && player.Postal == null) // both null
                postalIsSame = true;
            else if(!string.IsNullOrEmpty(Postal) && !string.IsNullOrEmpty(player.Postal)) // both non-null
            {
                postalIsSame = Postal.ToUpper() == player.Postal.ToUpper();
            }
            return FirstName == player.FirstName && MiddleName == player.MiddleName && LastName == player.LastName &&
                Address == player.Address && Country == player.Country && Community == player.Community &&
                Comments == player.Comments && Province == player.Province && postalIsSame
                && Email == player.Email && Phone == player.Phone;
        }

        public void SetFields(Player player)
        {
            FirstName = player.FirstName;
            MiddleName = player.MiddleName;
            LastName = player.LastName;
            Address = player.Address;
            Country = player.Country;
            Community = player.Community;
            Comments = player.Comments;
            Province = player.Province;
            Postal = player.Postal.ToUpper();
            Email = player.Email;
            Phone = player.Phone;
            LastUpdated = DateTime.Now;
        }

    }
}
