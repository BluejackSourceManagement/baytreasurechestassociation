﻿using System.Collections.Generic;

namespace BayTreasureChestAssociation.Models
{
    public sealed class SingletonDataObject
    {
        private static SingletonDataObject instance = null;
        private static readonly object padlock = new object();
        public static Dictionary<string, object> data = new Dictionary<string, object>();
        SingletonDataObject()
        {
        }
        public static SingletonDataObject Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new SingletonDataObject();
                    }
                    return instance;
                }
            }
        }
    }
}
