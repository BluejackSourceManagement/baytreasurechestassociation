﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BayTreasureChestAssociation.Models
{
    /// <summary>
    /// This class represents individual line items in the excel spread sheet
    /// it respresents the summary of a physical card
    /// </summary>
    public class DrawLineItemData
    {
        [Required]
        [Display(Name = "Id")]
        public int Id { get; set; }

        [Display(Name = "Sheet Number Start")]
        public int SheetNumberStart { get; set; }

        [Display(Name = "Sheet Number End")]
        public int SheetNumberEnd { get; set; }

        [Display(Name = "Sheet Entry Range")]
        public string SheetNumberRange { get; set; }

        [Display(Name = "Sticker Count")]
        public int StickerCount { get; set; }

        [Display(Name = "Question Sticker Count")]
        public int UnknownStickerCount { get; set; }

        [Display(Name = "Total Sticker Count")]
        public int TotalStickerCount { get; set; }



        //relationships



        //TODO -- QUESTION SHOULD THIS BE A COLLECTION ASSOCIATED WITH A DRAW?  PROBABLY NOT BECAUSE THIS WOULD NOT BE A PICK LIST
        //THIS WOULD MOST LIKELY BE A QUERY WHERE YOU QUERY FOR A GIVEN DRAW WEEK -- ie A DRAW THEN FOR THAT GIVEN DRAW
        //YOU LIST OUT ALL THE LINE ITEMS, BECAUSE A DRAW REALLY == A SHEET SO IT'S ITS OWN ENTITY THAT IS ASSOCIATED
        //WITH THE PHYSICAL ACTION OF SOME PEOPLE PERFORMING A DRAW
        //TABLED FOR NOW
        [Display(Name = "Draw ID")]
        public int? DrawID { get; set; }

        [Display(Name = "Draw ID")]
        [ForeignKey("DrawID")]
        public virtual Draw Draw { get; set; }
    }
}
