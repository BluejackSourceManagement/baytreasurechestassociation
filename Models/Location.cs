﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace BayTreasureChestAssociation.Models
{
    public class Location
    {
        public int Id { get; set; }
        [DisplayName("Location")]
        public string PlayLocation { get; set; }
        [DisplayName("Community")]
        public string Community { get; set; }
    }
}
