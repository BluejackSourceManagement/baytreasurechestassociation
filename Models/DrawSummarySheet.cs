﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BayTreasureChestAssociation.Models
{
    public class DrawSummarySheet
    {
        public Registration Registration { get; set; }
        public Draw Draw { get; set; }
        public CalendarDate CalendarDate { get; set; }
        public WinnerLog WinnerLog { get; set; }
        public Player Player { get; set; }
        public List<Location> Locations { get; set; }
    }
}
