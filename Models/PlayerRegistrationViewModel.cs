﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BayTreasureChestAssociation.Models
{
    public class PlayerRegistrationViewModel
    {
        public Player Player { get; set; }
        public Registration Registration { get; set; }
    }
}
