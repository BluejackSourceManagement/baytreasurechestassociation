﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BayTreasureChestAssociation.Models
{
    public class ApplicationSettings
    {
        public int Id { get; set; }
        [Display(Name ="NSAG License")]
        [Required]
        public string NSAGLicense { get; set; }
    }
}
