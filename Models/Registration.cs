﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BayTreasureChestAssociation.Models
{
    public class Registration
    {
        [Display(Name = "ID")]
        public int Id { get; set; }

        [Required]
        [Display(Name = "Registration Number")]
        public int RegNumber { get; set; }

        [Display(Name = "Status")]
        public string Status { get; set; }

        [Display(Name = "Registration Date")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime DateCreated { get; set; }

        [Display(Name = "Retirement Date")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? DateRetired { get; set; }

        [Display(Name = "Reg. Comments")]
        [DataType(DataType.Text)]
        public string Comments { get; set; }

        [Display(Name = "Player Id")]
        public int PlayerId { get; set; }

        // Navigation property
        [Display(Name = "Player")]
        public Player Player { get; set; }
    }
}
