﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BayTreasureChestAssociation.Models
{
    public class NonWinningDraw
    {
        public int Id { get; set; }

        [Display(Name = "Registration Number")]
        [Required]
        public int RegistrationNumber { get; set; }

        [Display(Name = "Week")]
        [Required]
        public int WeekNumber { get; set; }

        [Display(Name = "Draw Date")]
        [Required]
        public DateTime DrawDate { get; set; }

        [Display(Name = "Reason")]
        public string Reason { get; set; }
    }
}
