using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BayTreasureChestAssociation.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Options;

namespace BayTreasureChestAssociation.Areas.Identity.Pages.Account
{
    public class CreateDefaultAdminModel : PageModel
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly IOptions<AppSettings> settings;

        public CreateDefaultAdminModel(
            UserManager<IdentityUser> userManager, RoleManager<IdentityRole> roleManager, IOptions<AppSettings> settings)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            this.settings = settings;
        }
        public async Task<IActionResult> OnGet()
        {
            if (!await _roleManager.RoleExistsAsync(Utilities.StringValues.AppAdministrator))
            {
                await _roleManager.CreateAsync(new IdentityRole(Utilities.StringValues.AppAdministrator));
                ApplicationUser defaultAdmin = new ApplicationUser()
                {
                    UserName = settings.Value.DefaultAdminUserName,
                    Email = settings.Value.DefaultAdminEmail,
                    Name = settings.Value.DefaultAdminName,
                    PhoneNumber = settings.Value.DefaultAdminPhone,
                    EmailConfirmed = settings.Value.DefaultAdminEmailConfirmed
                };
                await _userManager.CreateAsync(defaultAdmin, settings.Value.DefaultAdminPassword);
                await _userManager.AddToRoleAsync(defaultAdmin, Utilities.StringValues.AppAdministrator);
            }
            if (!await _roleManager.RoleExistsAsync(Utilities.StringValues.AppEditor))
            {
                await _roleManager.CreateAsync(new IdentityRole(Utilities.StringValues.AppEditor));
            }
            if(!await _roleManager.RoleExistsAsync(Utilities.StringValues.ReadOnlyUser))
            {
                await _roleManager.CreateAsync(new IdentityRole(Utilities.StringValues.ReadOnlyUser));
            }

            return RedirectToAction("Index", "Dashboards", new { area = "Admin" });
        }
    }
}