﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BayTreasureChestAssociation.Data;
using BayTreasureChestAssociation.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BayTreasureChestAssociation.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Administrator,Editor,ReadOnly")]
    public class CountSummaryController : Controller
    {
        private readonly ApplicationDbContext _db;
        public CountSummaryController(ApplicationDbContext db)
        {
            _db = db;
        }

        public IActionResult Edit()
        {
            List<Draw> draws = _db.Draws.ToList();
            List<WinnerLog> winnerLogs = _db.WinnerLogs.ToList();
            List<CalendarDate> calendarDates = _db.CalendarDates.ToList();
            List<Registration> registrations = _db.Registrations.ToList();
            List<Player> players = _db.Players.ToList();
            // sort the draws by date
            if (draws != null)
            {
                draws.Sort((a, b) =>
                {
                    if (a.CalendarDateID > b.CalendarDateID)
                    {
                        return -1;
                    }
                    if (a.CalendarDateID < b.CalendarDateID)
                    {
                        return 1;
                    }
                    return 0;
                });
                // get the latest draw and add it to the summary sheet model
                Draw draw = draws[0];
                DrawSummarySheet drawSummarySheet = new DrawSummarySheet();
                drawSummarySheet.Draw = draw;
                // get the winner log for that draw and add it to the summary sheet model
                WinnerLog winnerLog = (from wl in winnerLogs
                                       where wl.DrawID == draw.Id
                                       select wl).FirstOrDefault();
                drawSummarySheet.WinnerLog = winnerLog;

                // set the locations list
                drawSummarySheet.Locations = _db.Locations.ToList();
                // get the calendar date and add it to the model
                drawSummarySheet.CalendarDate = (from cd in calendarDates
                                                 where cd.Id == draw.CalendarDateID
                                                 select cd).FirstOrDefault();
                // get the registration and add it to the model
                drawSummarySheet.Registration = (from r in registrations
                                                 where r.Id == draw.RegistrationID
                                                 select r).FirstOrDefault();
                // get the player and add it to the model
                drawSummarySheet.Player = (from p in players
                                           where p.Id == drawSummarySheet.Registration.PlayerId
                                           select p).FirstOrDefault();
                if (drawSummarySheet.Player != null)
                {
                    if (string.IsNullOrEmpty(drawSummarySheet.Player.Address)) drawSummarySheet.Player.Address = "Not Recorded.";
                    if (string.IsNullOrEmpty(drawSummarySheet.Player.Community)) drawSummarySheet.Player.Community = "Not Recorded.";
                }
                // pass the model to the view
                return View(drawSummarySheet);
            }
            // pass an empty model to the view
            return View(new DrawSummarySheet());
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator,Editor")]
        public async Task<IActionResult> Edit(DrawSummarySheet drawSummarySheet)
        {
            Draw drawFromDb = _db.Draws.Find(drawSummarySheet.Draw.Id);
            drawFromDb.BareCoins = drawSummarySheet.Draw.BareCoins;
            drawFromDb.CalendarDate = _db.CalendarDates.Where(cd => cd.Id == drawSummarySheet.CalendarDate.Id).FirstOrDefault();
            drawFromDb.CalendarDateID = drawSummarySheet.CalendarDate.Id;
            drawFromDb.Comments = drawSummarySheet.Draw.Comments;
            drawFromDb.DisqualifiedPinkSheetStickers = drawSummarySheet.Draw.DisqualifiedPinkSheetStickers;
            drawFromDb.ExpectedStickerCount = drawSummarySheet.Draw.ExpectedStickerCount;
            drawFromDb.HasWinner = drawSummarySheet.Draw.HasWinner;
            drawFromDb.KnownVariance = drawSummarySheet.Draw.KnownVariance;
            drawFromDb.NewRegistrations = drawSummarySheet.Draw.NewRegistrations;
            Registration reg = _db.Registrations.Where(r => r.Id == drawSummarySheet.Registration.Id).FirstOrDefault();
            drawFromDb.Registration = reg;
            drawFromDb.RegistrationID = reg.Id;
            drawFromDb.StickerCount = drawSummarySheet.Draw.StickerCount;
            drawFromDb.Supervisor = drawSummarySheet.Draw.Supervisor;
            drawFromDb.TotalStickerCount = drawSummarySheet.Draw.TotalStickerCount;
            drawFromDb.UnderStickeredDollarBills = drawSummarySheet.Draw.UnderStickeredDollarBills;
            drawFromDb.UnknownStickerCount = drawSummarySheet.Draw.UnknownStickerCount;
            _db.Update(drawFromDb);
            WinnerLog winnerLogFromDb = _db.WinnerLogs.Find(drawSummarySheet.WinnerLog.Id);
            winnerLogFromDb.Draw = _db.Draws.Find(drawSummarySheet.Draw.Id);
            winnerLogFromDb.DrawID = winnerLogFromDb.Draw.Id;
            winnerLogFromDb.RolloverAmount = drawSummarySheet.WinnerLog.RolloverAmount;
            winnerLogFromDb.RolloverCount = drawSummarySheet.WinnerLog.RolloverCount;
            winnerLogFromDb.WasPlayed = drawSummarySheet.WinnerLog.WasPlayed;
            winnerLogFromDb.ActualPrize = drawSummarySheet.WinnerLog.ActualPrize;
            winnerLogFromDb.BankDeposit = drawSummarySheet.WinnerLog.BankDeposit;
            winnerLogFromDb.ChequeNumber = drawSummarySheet.WinnerLog.ChequeNumber;
            winnerLogFromDb.Comments = drawSummarySheet.WinnerLog.Comments;
            winnerLogFromDb.Draw = drawFromDb;
            winnerLogFromDb.DrawID = drawFromDb.Id;
            winnerLogFromDb.HasRollover = drawSummarySheet.WinnerLog.HasRollover;
            winnerLogFromDb.LocationPlayed = drawSummarySheet.WinnerLog.LocationPlayed;
            winnerLogFromDb.PossiblePrize = drawSummarySheet.WinnerLog.PossiblePrize;
            winnerLogFromDb.RolloverAmount = drawSummarySheet.WinnerLog.RolloverAmount;
            winnerLogFromDb.RolloverCount = drawSummarySheet.WinnerLog.RolloverCount;
            if (!drawSummarySheet.Draw.HasWinner)
            {
                if (_db.WinnerLogs.Count() > 0)
                {
                    if (winnerLogFromDb.DrawID > 1)
                    {
                        WinnerLog previousLog = FindPreviousWinnerLog(drawFromDb.CalendarDate);
                        if (previousLog != null) winnerLogFromDb.RolloverCount = previousLog.RolloverCount + 1;
                    }
                }
                else
                    winnerLogFromDb.RolloverCount = 1;
            }
            _db.Update(winnerLogFromDb);
            await _db.SaveChangesAsync();
            return RedirectToAction(nameof(GetSummarySheetList));
        }

        [Authorize(Roles = "Administrator,Editor")]
        public IActionResult EditSheetFields(int id)
        {
            Draw draw = _db.Draws.Find(id);
            DrawUpdate drawUpdate = new DrawUpdate()
            {
                Comments = draw.Comments,
                DrawId = draw.Id,
                StickerCount = draw.StickerCount
            };
            return View(drawUpdate);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator,Editor")]
        public async Task<IActionResult> EditSheetFields(int id, DrawUpdate drawUpdate)
        {
            Draw draw = _db.Draws.Find(id);
            draw.Comments = drawUpdate.Comments;
            draw.StickerCount = drawUpdate.StickerCount;
            _db.Update(draw);
            await _db.SaveChangesAsync();
            return RedirectToAction(nameof(GetSummarySheetList));
        }

        public IActionResult GetSummarySheetList()
        {
            List<DrawSummarySheet> sheets = new List<DrawSummarySheet>();
            foreach (WinnerLog log in _db.WinnerLogs)
            {
                DrawSummarySheet sheet = new DrawSummarySheet();
                Draw draw = _db.Draws.Find(log.DrawID);
                Registration registration = _db.Registrations.Find(draw.RegistrationID);
                Player player = _db.Players.Find(registration.PlayerId);
                CalendarDate calendarDate = _db.CalendarDates.Find(draw.CalendarDateID);
                sheet.CalendarDate = calendarDate;
                sheet.Draw = draw;
                sheet.Player = player;
                sheet.Registration = registration;
                sheet.WinnerLog = log;
                sheet.Locations = _db.Locations.ToList();
                sheets.Add(sheet);
            }
            return View(sheets);
        }
        private string GetLatestWednesday()
        {
            string lastWednesday = string.Empty;
            DateTime now = DateTime.Now;
            if (now.DayOfWeek == DayOfWeek.Wednesday)
            {
                lastWednesday = now.ToShortDateString();
            }
            else
            {
                // go back a week
                now = now.Subtract(TimeSpan.FromDays(7.0));
                for (int i = 0; i < 7; i++)
                {
                    if (now.DayOfWeek == DayOfWeek.Wednesday)
                    {
                        break;
                    }
                    else
                    {
                        // keep adding days until you find Wednesday
                        now = now.AddDays(1.0);
                    }
                }
                lastWednesday = now.ToShortDateString();
            }
            return lastWednesday;
        }

        public JsonResult GetDrawDate(int week)
        {
            if (week >= 1 && week <= 500)
            {
                CalendarDate cd = _db.CalendarDates.Where(c => c.WeekNumber == week).FirstOrDefault();
                if (cd != null)
                {
                    return Json(cd.DateStart);
                }
                else
                {
                    return Json("Unable to find Calendar Date");
                }
            }
            else
                return Json("Invalid week number.");
        }

        public JsonResult GetRollover(int week)
        {
            if (week >= 1 && week <= 500)
            {
                // need to get the Calendar date id 
                // then find the Draw with that foreign key
                // then get the Draw Id and then get the 
                // winner log with that id in order to find
                // the rollover amount
                CalendarDate calendarDate = _db.CalendarDates.Where(c => c.WeekNumber == week).FirstOrDefault();
                if (calendarDate == null) return Json("Calendar Date not found.");
                Draw draw = _db.Draws.Where(d => d.CalendarDateID == calendarDate.Id).FirstOrDefault();
                if (draw == null) return Json(0.0);
                if (draw.HasWinner) return Json(0.0);
                WinnerLog winnerLog = _db.WinnerLogs.Where(w => w.DrawID == draw.Id).FirstOrDefault();
                if (winnerLog == null) return Json(0.0);
                return Json(winnerLog.RolloverAmount);
            }
            else
                return Json("Draw week out of range of acceptable values.");
        }
        [Authorize(Roles = "Administrator,Editor")]
        public IActionResult Index()
        {
            string lastWednesday = GetLatestWednesday();
            CalendarDate currentCalendarDate = _db.CalendarDates.Where(cd => cd.DateStart.ToShortDateString() == lastWednesday).FirstOrDefault();
            Player player = new Player();
            Registration registration = new Registration();
            Draw draw = new Draw();
            WinnerLog winnerLog = new WinnerLog();
            List<Location> locations = _db.Locations.ToList();
            DrawSummarySheet ds = new DrawSummarySheet()
            {
                Player = player,
                CalendarDate = currentCalendarDate,
                Draw = draw,
                WinnerLog = winnerLog,
                Locations = locations,
                Registration = registration
            };
            return View(ds);
        }
        /// <summary>
        /// If there is no winner then the rollover amount should be last week's
        /// amount + this week's prize and this weeks prize should be empty
        /// otherwise the rollover amount should be last week's + this week's
        /// </summary>
        /// <param name="drawSummarySheet"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator,Editor")]
        public async Task<IActionResult> Index(DrawSummarySheet drawSummarySheet)
        {
            Registration reg = _db.Registrations.Where(r => r.RegNumber == drawSummarySheet.Registration.RegNumber).FirstOrDefault();
            Draw draw = drawSummarySheet.Draw;
            // need to check if draw exists

            draw.Registration = reg;
            draw.RegistrationID = reg.Id;
            CalendarDate cd = _db.CalendarDates.Where(c => c.WeekNumber == drawSummarySheet.CalendarDate.WeekNumber).FirstOrDefault();
            draw.CalendarDate = cd;
            draw.CalendarDateID = cd.Id;
            List<Draw> draws = _db.Draws.ToList();
            Draw draw1 = draws.Where(d => d.CalendarDateID == draw.CalendarDateID).FirstOrDefault();
            if(draw1 != null) return RedirectToAction(nameof(Index));
            _db.Draws.Add(draw);
            await _db.SaveChangesAsync();
            // need winner log info
            WinnerLog winnerLog = drawSummarySheet.WinnerLog;
            winnerLog.Draw = draw;
            winnerLog.DrawID = _db.Draws.Where(d => d.CalendarDateID == draw.CalendarDateID).FirstOrDefault().Id;
            if (draw.HasWinner)
            {
                winnerLog.RolloverAmount = 0;
                winnerLog.RolloverCount = 0;
                winnerLog.WasPlayed = true;
                // this has to be reviewed
                // right now it will load the community if specified
                // if not it tries to find an address if specified
                // otherwise it loads "Unknown."
                winnerLog.LocationPlayed = drawSummarySheet.WinnerLog.LocationPlayed;
            }
            else
            {
                winnerLog.WasPlayed = false;
                if (_db.WinnerLogs.Count() > 0)
                {
                    if (winnerLog.DrawID > 1)
                    {
                        WinnerLog previousLog = FindPreviousWinnerLog(cd);
                        if (previousLog != null) winnerLog.RolloverCount = previousLog.RolloverCount + 1;
                    }
                }
                else
                    winnerLog.RolloverCount = 1;
            }
            _db.WinnerLogs.Add(winnerLog);
            await _db.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private WinnerLog FindPreviousWinnerLog(CalendarDate calendarDate)
        {
            Draw draw = _db.Draws.Where(d => d.CalendarDateID == calendarDate.Id - 1).FirstOrDefault();
            if (draw == null) return null;
            WinnerLog previousLog = _db.WinnerLogs.Where(w => w.DrawID == draw.Id).FirstOrDefault();
            return previousLog;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator,Editor")]
        public IActionResult AddNewCountSummary()
        {
            return NotFound();
        }

        public JsonResult IsDuplicateRegNum(string RegNumber)
        {
            // can't validate
            if (RegNumber == "NaN") return Json(true);
            /// Note: The parameter name must match the spelling of the actual 
            /// property in the registration object 
            Registration validateRegNum = _db.Registrations.FirstOrDefault(reg => reg.RegNumber == Convert.ToInt32(RegNumber));
            if (validateRegNum != null) return Json(true);
            return Json(false);
        }

        public JsonResult GetPlayerData(string regNum)
        {
            if (regNum != null)
            {
                int registrationNumber = Convert.ToInt32(regNum);
                Registration reg = _db.Registrations.Where(r => r.RegNumber == registrationNumber).FirstOrDefault();
                if (reg != null)
                {
                    Draw draw = _db.Draws.Where(d => d.RegistrationID == reg.Id).FirstOrDefault();
                    WinnerLog winnerLog = null;
                    if (draw != null)
                        winnerLog = _db.WinnerLogs.Where(wl => wl.DrawID == draw.Id).FirstOrDefault();
                    Player player = _db.Players.Find(reg.PlayerId);
                    string name = player.FirstName + ((player.MiddleName != null) ? " " + player.MiddleName : "") + ((player.LastName != null) ? " " + player.LastName : "");
                    string community = string.IsNullOrEmpty(player.Community) ? "Not recorded." : player.Community;
                    string address = string.IsNullOrEmpty(player.Address) ? "Not recorded." : player.Address;
                    string phone = string.IsNullOrEmpty(player.Phone) ? "Not recorded." : player.Phone;
                    string email = string.IsNullOrEmpty(player.Email) ? "Not recorded." : player.Email;
                    string location = winnerLog != null ? winnerLog.LocationPlayed : "";
                    string check = winnerLog != null ? winnerLog.ChequeNumber : "";
                    if (player != null)
                    {
                        var playerData = new
                        {
                            name = name,
                            community = community,
                            address = address,
                            phone = phone,
                            email = email,
                            location = location,
                            check = check
                        };
                        return Json(playerData);
                    }
                    else
                        return Json("No player found.");
                }
                else
                    return Json("No registration found.");
            }
            else
                return Json("Null registration number.");
        }

        public JsonResult DrawHasBeenRecorded(int weekNum)
        {
            CalendarDate calendarDate = _db.CalendarDates.Where(c => c.WeekNumber == weekNum).FirstOrDefault();
            if (calendarDate == null) return Json(false);
            Draw draw = _db.Draws.Where(d => d.CalendarDateID == calendarDate.Id).FirstOrDefault();
            return Json(draw != null);
        }
        [Authorize(Roles = "Administrator,Editor")]
        public IActionResult DeleteCountSummary(int winnerLogId)
        {
            WinnerLog log = _db.WinnerLogs.Find(winnerLogId);
            int? drawId = log.DrawID;
            Draw draw = _db.Draws.Find(drawId);
            _db.WinnerLogs.Remove(log);
            _db.Draws.Remove(draw);
            _db.SaveChanges();
            return View();
        }

        public JsonResult GetLastWednesdaysCalendarDate()
        {
            string lastWednesday = GetLatestWednesday();
            List<CalendarDate> calendarDates = _db.CalendarDates.ToList();
            CalendarDate lastWednesdaysCalendarDate = calendarDates.Where(cd => cd.DateStart.ToShortDateString() == lastWednesday).FirstOrDefault();
            return Json(lastWednesdaysCalendarDate.WeekNumber);
        }
    }
}