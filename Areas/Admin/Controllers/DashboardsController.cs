﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BayTreasureChestAssociation.Data;
using BayTreasureChestAssociation.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BayTreasureChestAssociation.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Administrator,Editor,ReadOnly")]
    public class DashboardsController : Controller
    {
        private readonly ApplicationDbContext _db;
        public DashboardsController(ApplicationDbContext db)
        {
            _db = db;
        }


        private List<PlayerRegistration> GetRetiredPlayerRegistrations(string date)
        {
            IEnumerable<Registration> registrations = _db.Registrations;
            IEnumerable<Player> players = _db.Players;
            IEnumerable<PlayerRegistration> playerRegs =
                from reg in registrations
                join player in players on reg.PlayerId equals player.Id
                where reg.DateCreated.ToShortDateString() == GetDate(date).ToShortDateString()
                && reg.Status == "Retired"
                select new PlayerRegistration
                {
                    Player = player,
                    Registration = reg
                };

            return playerRegs.ToList();
        }
        public IActionResult ChooseView(string dateStart, string dateEnd, string viewType)
        {
            return RedirectToAction("Index", new { searchDateStart = dateStart, searchDateEnd = dateEnd });
        }
        private AllRegistrationsViewModel GetAllRegistrationsViewModel(CalendarDate calendarDate, List<Draw> draws, List<Registration> registrations, SearchType searchType)
        {
            AllRegistrationsViewModel allRegistrationsViewModel = new AllRegistrationsViewModel();
            int activeRegistrations = (from reg in registrations
                                       where reg.DateCreated <= calendarDate.DateStart && reg.Status == "Active"
                                       select reg).Count();
            // Get those retired regs from the week prior to the current date
            int retiredRegistrations = (from regRet in registrations
                                        where regRet.DateRetired != null && (DateTime)regRet.DateRetired <= calendarDate.DateStart && ((DateTime)regRet.DateRetired) >= calendarDate.DateStart.Subtract(TimeSpan.FromDays(6.999))
                                        select regRet).Count();

            int newRegistrations = (from regNew in registrations
                                    where regNew.DateCreated.ToShortDateString() == calendarDate.DateStart.ToShortDateString()
                                    select regNew).Count();

            Draw currentDraw = (from draw in draws
                                where draw.CalendarDateID == calendarDate.Id
                                select draw).FirstOrDefault();
            int playedRegistrations = currentDraw == null ? 0 : currentDraw.StickerCount;
            allRegistrationsViewModel.NewRegistrations = newRegistrations;
            allRegistrationsViewModel.RetiredRegistrations = retiredRegistrations;
            allRegistrationsViewModel.TotalActive = activeRegistrations;
            allRegistrationsViewModel.TotalPlayed = playedRegistrations;
            allRegistrationsViewModel.Week = calendarDate.DateStart.ToShortDateString();
            allRegistrationsViewModel.SearchType = searchType;
            return allRegistrationsViewModel;
        }
        public IActionResult AllRegistrations(string searchDateStart = null, string searchDateEnd = null)
        {
            List<AllRegistrationsViewModel> allRegistrationsViewModels = new List<AllRegistrationsViewModel>();
            List<CalendarDate> calendarDates = _db.CalendarDates.ToList();
            List<Registration> registrations = _db.Registrations.ToList();
            registrations = registrations.Where(r => r.Status != "Other").ToList();
            List<Draw> draws = _db.Draws.ToList();
            if (searchDateStart == null && searchDateEnd == null)
            {
                DateTime lastWednesday = GetLastWednesday();
                CalendarDate date = (from cd in calendarDates
                                     where cd.DateStart.ToShortDateString() == lastWednesday.ToShortDateString()
                                     select cd).FirstOrDefault();
                DateTime weekBefore = lastWednesday.Subtract(TimeSpan.FromDays(7.0));
                CalendarDate weekBeforeCD = (from cd in calendarDates
                                             where cd.DateStart.ToShortDateString() == weekBefore.ToShortDateString()
                                             select cd).FirstOrDefault();
                DateTime twoWeeksBefore = lastWednesday.Subtract(TimeSpan.FromDays(14.0));
                CalendarDate twoWeeksBeforeCD = (from cd in calendarDates
                                                 where cd.DateStart.ToShortDateString() == twoWeeksBefore.ToShortDateString()
                                                 select cd).FirstOrDefault();
                DateTime threeWeeksBefore = lastWednesday.Subtract(TimeSpan.FromDays(21.0));
                CalendarDate threeWeeksBeforeCD = (from cd in calendarDates
                                                   where cd.DateStart.ToShortDateString() == threeWeeksBefore.ToShortDateString()
                                                   select cd).FirstOrDefault();
                // need to go back four weeks for default view
                allRegistrationsViewModels.Add(GetAllRegistrationsViewModel(date, draws, registrations, SearchType.BothNull));
                allRegistrationsViewModels.Add(GetAllRegistrationsViewModel(weekBeforeCD, draws, registrations, SearchType.BothNull ));
                allRegistrationsViewModels.Add(GetAllRegistrationsViewModel(twoWeeksBeforeCD, draws, registrations, SearchType.BothNull));
                allRegistrationsViewModels.Add(GetAllRegistrationsViewModel(threeWeeksBeforeCD, draws, registrations, SearchType.BothNull));
            }
            else if (searchDateStart != null && searchDateEnd != null)
            {
                DateTime dateTime = GetDate(searchDateStart);
                CalendarDate calDate = null;
                if (dateTime.DayOfWeek != DayOfWeek.Wednesday)
                {
                    calDate =  GetNearestPastCalendarDate(calendarDates, dateTime);
                    searchDateStart = calDate.DateStart.ToShortDateString();
                }
                List<CalendarDate> searchDates = (from cd in calendarDates
                                                  where cd.DateStart >= GetDate(searchDateStart) && cd.DateStart <= GetDate(searchDateEnd)
                                                  select cd).ToList();
                if (searchDates != null && searchDates.Count() > 0)
                {
                    foreach (CalendarDate calendarDate in searchDates)
                    {
                        AllRegistrationsViewModel allRegistrationsViewModel = GetAllRegistrationsViewModel(calendarDate, draws, registrations, SearchType.RangeDefined);
                        allRegistrationsViewModels.Add(allRegistrationsViewModel);
                    }
                }
            }
            else if (searchDateStart != null)
            {
                DateTime searchStart = GetDate(searchDateStart);
                CalendarDate startCalendarDate = null;
                if(searchStart.DayOfWeek != DayOfWeek.Wednesday)
                {
                    startCalendarDate = GetNearestPastCalendarDate(calendarDates, searchStart);
                }
                else
                {
                    startCalendarDate = (from cd in calendarDates
                                         where cd.DateStart.ToShortDateString() == GetDate(searchDateStart).ToShortDateString()
                                         select cd).FirstOrDefault();
                }
                AllRegistrationsViewModel allRegistrationsViewModel = GetAllRegistrationsViewModel(startCalendarDate, draws, registrations, SearchType.StartDateOnly);
                allRegistrationsViewModels.Add(allRegistrationsViewModel);
            }
            else if (searchDateEnd != null)
            {
                DateTime searchEnd = GetDate(searchDateEnd);
                CalendarDate endCalendarDate = null;
                if (searchEnd.DayOfWeek != DayOfWeek.Wednesday)
                {
                    endCalendarDate = GetNearestPastCalendarDate(calendarDates, searchEnd);
                }
                else
                {
                    endCalendarDate = (from cd in calendarDates
                                         where cd.DateStart.ToShortDateString() == GetDate(searchDateStart).ToShortDateString()
                                         select cd).FirstOrDefault();
                }
                AllRegistrationsViewModel allRegistrationsViewModel = GetAllRegistrationsViewModel(endCalendarDate, draws, registrations, SearchType.EndDateOnly);
                allRegistrationsViewModels.Add(allRegistrationsViewModel);
            }
            allRegistrationsViewModels = allRegistrationsViewModels.Reverse<AllRegistrationsViewModel>().ToList();
            return View(allRegistrationsViewModels);
        }
        private List<PlayerRegistration> GetRetiredRegistrations(CalendarDate calendarDate, List<Registration> registrations, List<Player> players, SearchType searchType)
        {
            // get those registrations that were retired during the period
            // 6.999 days back from the Draw Date
            IEnumerable<Registration> retiredRegistrations = from regRet in registrations
                                                             where regRet.DateRetired != null && (DateTime)regRet.DateRetired <= calendarDate.DateStart && ((DateTime)regRet.DateRetired) >= calendarDate.DateStart.Subtract(TimeSpan.FromDays(6.999))
                                                             select regRet;
            // do a join on the players to get the PlayerRegistrations
            IEnumerable<PlayerRegistration> playerRegs =
            from reg in retiredRegistrations
            join player in players on reg.PlayerId equals player.Id
            select new PlayerRegistration
            {
                Player = player,
                Registration = reg,
                SearchType = searchType
            };
            return playerRegs.ToList();
        }

        private CalendarDate GetNearestFutureCalendarDate(List<CalendarDate> calendarDates, DateTime dateTime)
        {
            CalendarDate calendarDate = null;
            if (dateTime.DayOfWeek == DayOfWeek.Wednesday)
                calendarDate = (from cd in calendarDates
                                where cd.DateStart.ToShortDateString() == dateTime.ToShortDateString()
                                select cd).FirstOrDefault();
            else
            {

                for (int i = 1; i < 7; i++)
                {
                    DateTime nextDay = dateTime.AddDays(i);
                    if (nextDay.DayOfWeek == DayOfWeek.Wednesday)
                    {
                        calendarDate = (from cd in calendarDates
                                        where cd.DateStart.ToShortDateString() == nextDay.ToShortDateString()
                                        select cd).FirstOrDefault();
                    }
                }
            }
            return calendarDate;
        }
        private CalendarDate GetNearestPastCalendarDate(List<CalendarDate> calendarDates, DateTime dateTime)
        {
            CalendarDate calendarDate = null;
            if (dateTime.DayOfWeek == DayOfWeek.Wednesday)
                calendarDate = (from cd in calendarDates
                                where cd.DateStart.ToShortDateString() == dateTime.ToShortDateString()
                                select cd).FirstOrDefault();
            else
            {

                for (int i = 1; i < 7; i++)
                {
                    DateTime lastDay = dateTime.Subtract(TimeSpan.FromDays(i));
                    if (lastDay.DayOfWeek == DayOfWeek.Wednesday)
                    {
                        calendarDate = (from cd in calendarDates
                                        where cd.DateStart.ToShortDateString() == lastDay.ToShortDateString()
                                        select cd).FirstOrDefault();
                    }
                }
            }
            return calendarDate;
        }
        public IActionResult RetiredRegistrations(string searchDateStart = null, string searchDateEnd = null)
        {
            List<PlayerRegistration> retiredRegistrationsViewModels = new List<PlayerRegistration>();
            List<CalendarDate> calendarDates = _db.CalendarDates.ToList();
            List<Registration> registrations = _db.Registrations.ToList();
            List<Player> players = _db.Players.ToList();
            List<Draw> draws = _db.Draws.ToList();
            if (searchDateStart == null && searchDateEnd == null)
            {
                IEnumerable<Registration> retiredRegistrations = from reg in registrations
                                                                 where reg.DateRetired != null &&
                                                                 (DateTime)reg.DateRetired <= DateTime.Now &&
                                                                 (DateTime)reg.DateRetired >= DateTime.Now.Subtract(TimeSpan.FromDays(28))
                                                                 select reg;
                IEnumerable<PlayerRegistration> playerRegs =
                from reg in retiredRegistrations
                join player in players on reg.PlayerId equals player.Id
                select new PlayerRegistration
                {
                    Player = player,
                    Registration = reg,
                    SearchType = SearchType.BothNull
                };
                retiredRegistrationsViewModels = playerRegs.ToList();
            }
            else if (searchDateStart != null && searchDateEnd != null)
            {
                IEnumerable<Registration> retiredRegistrations = from reg in registrations
                                                                 where reg.DateRetired != null &&
                                                                 (DateTime)reg.DateRetired <= GetDate(searchDateEnd) &&
                                                                 (DateTime)reg.DateRetired >= GetDate(searchDateStart)
                                                                 select reg;
                IEnumerable<PlayerRegistration> playerRegs = from reg in retiredRegistrations
                                                            join player in players on reg.PlayerId equals player.Id
                                                            select new PlayerRegistration
                                                            {
                                                                Player = player,
                                                                Registration = reg,
                                                                SearchType = SearchType.RangeDefined
                                                            };
                retiredRegistrationsViewModels = playerRegs.ToList();
            }
            else if (searchDateStart != null)
            {
                IEnumerable<Registration> retiredRegistrations = from reg in registrations
                                                                 where reg.DateRetired != null &&
                                                                 ((DateTime)reg.DateRetired).ToShortDateString()== GetDate(searchDateStart).ToShortDateString()
                                                                 select reg;
                IEnumerable<PlayerRegistration> playerRegs = from reg in retiredRegistrations
                                                             join player in players on reg.PlayerId equals player.Id
                                                             select new PlayerRegistration
                                                             {
                                                                 Player = player,
                                                                 Registration = reg,
                                                                 SearchType = SearchType.StartDateOnly
                                                             };
                retiredRegistrationsViewModels = playerRegs.ToList();
            }
            else if (searchDateEnd != null)
            {
                IEnumerable<Registration> retiredRegistrations = from reg in registrations
                                                                 where reg.DateRetired != null &&
                                                                 ((DateTime)reg.DateRetired).ToShortDateString() == GetDate(searchDateEnd).ToShortDateString()
                                                                 select reg;
                IEnumerable<PlayerRegistration> playerRegs = from reg in retiredRegistrations
                                                             join player in players on reg.PlayerId equals player.Id
                                                             select new PlayerRegistration
                                                             {
                                                                 Player = player,
                                                                 Registration = reg,
                                                                 SearchType = SearchType.EndDateOnly
                                                             };
                retiredRegistrationsViewModels = playerRegs.ToList();
            }
            retiredRegistrationsViewModels.Sort((a, b) =>
            {
                if (a.Registration.DateRetired > b.Registration.DateRetired) return 1;
                if (a.Registration.DateRetired < b.Registration.DateRetired) return -1;
                return 0;
            });
            return View(retiredRegistrationsViewModels);
        }
        private NewRegistrationListViewModel GetNewRegistrations(CalendarDate calendarDate, List<Draw> draws, List<Registration> registrations, List<Player> players)
        {
            NewRegistrationListViewModel vm = new NewRegistrationListViewModel();
            IEnumerable<Registration> filteredRegs = from reg in registrations
                                                     where reg.DateCreated.ToShortDateString() == calendarDate.DateStart.ToShortDateString()
                                                     select reg;
            IEnumerable<PlayerRegistration> playerRegistrations = from regs in filteredRegs
                                                                  join player in players on regs.PlayerId equals player.Id
                                                                  select new PlayerRegistration()
                                                                  {
                                                                      Player = player,
                                                                      Registration = regs
                                                                  };
            vm.PlayerRegistrations = playerRegistrations.ToList();
            vm.RegistrationDate = calendarDate.DateStart;
            vm.SearchType = SearchType.BothNull;

            return vm;
        }
        private List<NewRegistrationListViewModel> GetNewRegistrationListViewModel(string date, SearchType searchType)
        {
            IEnumerable<Registration> registrations = _db.Registrations;
            registrations = registrations.Where(r => r.Status != "Other");
            IEnumerable<CalendarDate> calendarDates = _db.CalendarDates;
            IEnumerable<Player> players = _db.Players;
            registrations = from reg in registrations
                            where reg.DateCreated.ToShortDateString() == GetDate(date).ToShortDateString()
                            select reg;
            IEnumerable<PlayerRegistration> playerRegs = from reg in registrations
                                                         join player in players on reg.PlayerId equals player.Id
                                                         select new PlayerRegistration
                                                         {
                                                             Player = player,
                                                             Registration = reg
                                                         };

            List<NewRegistrationListViewModel> viewModels = new List<NewRegistrationListViewModel>();
            NewRegistrationListViewModel viewModel = new NewRegistrationListViewModel();
            viewModel.RegistrationDate = GetDate(date);
            viewModel.SearchType = searchType;
            viewModel.PlayerRegistrations = playerRegs.ToList();
            viewModels.Add(viewModel);
            return viewModels;
        }
        public IActionResult NewRegistrations(string searchDateStart = null, string searchDateEnd = null)
        {
            List<Registration> registrations = _db.Registrations.ToList();
            registrations = registrations.Where(r => r.Status != "Other").ToList();
            List<CalendarDate> calendarDates = _db.CalendarDates.ToList();
            List<Player> players = _db.Players.ToList();
            List<Draw> draws = _db.Draws.ToList();
            // need to know the details for each registration with date created
            // equal to the search date
            if (searchDateEnd == null && searchDateStart == null)
            {
                List<NewRegistrationListViewModel> newRegistrationViewModels = new List<NewRegistrationListViewModel>();
                DateTime lastWednesday = GetLastWednesday();
                CalendarDate date = (from cd in calendarDates
                                     where cd.DateStart.ToShortDateString() == lastWednesday.ToShortDateString()
                                     select cd).FirstOrDefault();
                DateTime weekBefore = lastWednesday.Subtract(TimeSpan.FromDays(7.0));
                CalendarDate weekBeforeCD = (from cd in calendarDates
                                             where cd.DateStart.ToShortDateString() == weekBefore.ToShortDateString()
                                             select cd).FirstOrDefault();
                DateTime twoWeeksBefore = lastWednesday.Subtract(TimeSpan.FromDays(14.0));
                CalendarDate twoWeeksBeforeCD = (from cd in calendarDates
                                                 where cd.DateStart.ToShortDateString() == twoWeeksBefore.ToShortDateString()
                                                 select cd).FirstOrDefault();
                DateTime threeWeeksBefore = lastWednesday.Subtract(TimeSpan.FromDays(21.0));
                CalendarDate threeWeeksBeforeCD = (from cd in calendarDates
                                                   where cd.DateStart.ToShortDateString() == threeWeeksBefore.ToShortDateString()
                                                   select cd).FirstOrDefault();
                // need to go back four weeks for default view
                newRegistrationViewModels.Add(GetNewRegistrations(date, draws, registrations, players));
                newRegistrationViewModels.Add(GetNewRegistrations(weekBeforeCD, draws, registrations, players));
                newRegistrationViewModels.Add(GetNewRegistrations(twoWeeksBeforeCD, draws, registrations, players));
                newRegistrationViewModels.Add(GetNewRegistrations(threeWeeksBeforeCD, draws, registrations, players));
                newRegistrationViewModels = newRegistrationViewModels.Reverse<NewRegistrationListViewModel>().ToList();
                return View(newRegistrationViewModels);
            }
            else if (searchDateStart != null && searchDateEnd != null)
            {
                List<NewRegistrationListViewModel> newRegistrationListViewModels = new List<NewRegistrationListViewModel>();
                DateTime startDate = GetDate(searchDateStart);
                CalendarDate startCalendarDate = null;
                if (startDate.DayOfWeek != DayOfWeek.Wednesday)
                    startCalendarDate = GetNearestPastCalendarDate(calendarDates, startDate);
                else
                    startCalendarDate = calendarDates.Where(cd => cd.DateStart.ToShortDateString() == startDate.ToShortDateString()).FirstOrDefault();
                DateTime endDate = GetDate(searchDateEnd);
                CalendarDate endCalendarDate = null;
                if (endDate.DayOfWeek != DayOfWeek.Wednesday)
                    endCalendarDate = GetNearestPastCalendarDate(calendarDates, endDate);
                else
                    endCalendarDate = calendarDates.Where(cd => cd.DateStart.ToShortDateString() == endDate.ToShortDateString()).FirstOrDefault();

                calendarDates = calendarDates.Where(cd => cd.DateStart >= startCalendarDate.DateStart && cd.DateStart <= endCalendarDate.DateStart).ToList();
                foreach (CalendarDate cd in calendarDates)
                {
                    newRegistrationListViewModels.AddRange(GetNewRegistrationListViewModel(cd.DateStart.ToShortDateString(), SearchType.RangeDefined).ToList());
                }
                return View(newRegistrationListViewModels);
            }
            else if (searchDateEnd != null)
            {
                DateTime date = GetDate(searchDateEnd);
                if (date.DayOfWeek != DayOfWeek.Wednesday)
                {
                    CalendarDate cd = GetNearestPastCalendarDate(calendarDates, date);
                    return View(GetNewRegistrationListViewModel(cd.DateStart.ToShortDateString(), SearchType.EndDateOnly));
                }
                else
                {
                    return View(GetNewRegistrationListViewModel(searchDateEnd, SearchType.EndDateOnly));
                }
            }
            else if (searchDateStart != null)
            {
                DateTime date = GetDate(searchDateStart);
                if (date.DayOfWeek != DayOfWeek.Wednesday)
                {
                    CalendarDate cd = GetNearestPastCalendarDate(calendarDates, date);
                    return View(GetNewRegistrationListViewModel(cd.DateStart.ToShortDateString(), SearchType.StartDateOnly));
                }
                else
                {
                    return View(GetNewRegistrationListViewModel(searchDateStart, SearchType.StartDateOnly));
                }
            }
            return View(new List<NewRegistrationListViewModel>());
        }
        public IActionResult Index(string searchDateStart = null, string searchDateEnd = null)
        {
            List<ActiveRegistrationViewModel> activeRegistrationsViewModels = new List<ActiveRegistrationViewModel>();
            List<CalendarDate> calendarDates = _db.CalendarDates.ToList();

            List<Draw> draws = _db.Draws.ToList();
            if (searchDateStart == null && searchDateEnd == null)
            {
                List<Registration> registrations = _db.Registrations.ToList();
                DateTime lastWednesday = GetLastWednesday();
                CalendarDate date = (from cd in calendarDates
                                     where cd.DateStart.ToShortDateString() == lastWednesday.ToShortDateString()
                                     select cd).FirstOrDefault();
                DateTime weekBefore = lastWednesday.Subtract(TimeSpan.FromDays(7.0));
                CalendarDate weekBeforeCD = (from cd in calendarDates
                                             where cd.DateStart.ToShortDateString() == weekBefore.ToShortDateString()
                                             select cd).FirstOrDefault();
                DateTime twoWeeksBefore = lastWednesday.Subtract(TimeSpan.FromDays(14.0));
                CalendarDate twoWeeksBeforeCD = (from cd in calendarDates
                                                 where cd.DateStart.ToShortDateString() == twoWeeksBefore.ToShortDateString()
                                                 select cd).FirstOrDefault();
                DateTime threeWeeksBefore = lastWednesday.Subtract(TimeSpan.FromDays(21.0));
                CalendarDate threeWeeksBeforeCD = (from cd in calendarDates
                                                   where cd.DateStart.ToShortDateString() == threeWeeksBefore.ToShortDateString()
                                                   select cd).FirstOrDefault();
                // need to go back four weeks for default view
                activeRegistrationsViewModels.Add(GetActiveRegistrationsViewModel(date, draws, registrations, SearchType.BothNull));
                activeRegistrationsViewModels.Add(GetActiveRegistrationsViewModel(weekBeforeCD, draws, registrations, SearchType.BothNull));
                activeRegistrationsViewModels.Add(GetActiveRegistrationsViewModel(twoWeeksBeforeCD, draws, registrations, SearchType.BothNull));
                activeRegistrationsViewModels.Add(GetActiveRegistrationsViewModel(threeWeeksBeforeCD, draws, registrations, SearchType.BothNull));
                return View(activeRegistrationsViewModels);
            }
            else if (searchDateEnd != null && searchDateStart != null)
            {
                DateTime start = GetDate(searchDateStart);
                CalendarDate calDate = null;
                if(start.DayOfWeek != DayOfWeek.Wednesday)
                {
                    calDate = GetNearestPastCalendarDate(calendarDates, start);
                    start = calDate.DateStart;
                }
                DateTime end = GetDate(searchDateEnd);
                IEnumerable<CalendarDate> cDates = _db.CalendarDates.Where(cd => cd.DateStart >= start && cd.DateStart <= end);
                IEnumerable<Registration> registrations = _db.Registrations.Where(r => r.Status == "Active" && r.DateCreated <= end);
                IEnumerable<DrawCalendarDate> drawDateItems =  from d in draws
                                                               join c in cDates on d.CalendarDateID equals c.Id
                                                               select new DrawCalendarDate
                                                               {
                                                                   Draw = d,
                                                                   CalendarDate = c
                                                               };
                if(cDates.Count() != drawDateItems.Count())
                {
                    List<DrawCalendarDate> list = new List<DrawCalendarDate>();
                    // need some bogus draws
                    bool hasDraw = false;
                    foreach(CalendarDate calendarDate in cDates)
                    {
                        hasDraw = false;
                        DrawCalendarDate drawCalendarDate = new DrawCalendarDate();
                        foreach(DrawCalendarDate dcd in drawDateItems)
                        {
                            if(dcd.CalendarDate.Id == calendarDate.Id)
                            {
                                hasDraw = true;
                                break;
                            }
                        }
                        if (hasDraw) continue;
                        else
                        {
                            drawCalendarDate.CalendarDate = calendarDate;
                            drawCalendarDate.Draw = new Draw() { StickerCount = 0 };
                            list.Add(drawCalendarDate);
                            activeRegistrationsViewModels.Add(GetActiveRegistrationsViewModel(calendarDate, draws, registrations.ToList(), SearchType.RangeDefined));
                        }
                    }
                }

                foreach (DrawCalendarDate item in drawDateItems)
                {
                    activeRegistrationsViewModels.Add(GetActiveRegistrationsViewModel(item.CalendarDate, draws, registrations.ToList(), SearchType.RangeDefined));
                }
                activeRegistrationsViewModels = activeRegistrationsViewModels.OrderBy(a => a.WeekNumber).ToList();
                activeRegistrationsViewModels = activeRegistrationsViewModels.Reverse<ActiveRegistrationViewModel>().ToList();
                return View(activeRegistrationsViewModels);
            }
            else if (searchDateStart != null)
            {
                DateTime wednesday = GetNearestPastCalendarDate(calendarDates, GetDate(searchDateStart)).DateStart;

                IEnumerable<Registration> registrations = _db.Registrations.Where(r => r.Status == "Active");
                CalendarDate date = _db.CalendarDates.Where(cd => cd.DateStart.ToShortDateString() == wednesday.ToShortDateString()).FirstOrDefault();
                Draw draw = _db.Draws.Where(d => d.CalendarDateID == date.Id).FirstOrDefault();
                int regCount = (from reg in registrations
                                where reg.DateCreated <= date.DateStart
                                select reg).Count();
                ActiveRegistrationViewModel arvm = new ActiveRegistrationViewModel();
                arvm.Total = regCount;
                arvm.TotalPlayed = (draw != null) ? draw.StickerCount : 0;
                arvm.Week = date.DateStart.ToShortDateString();
                arvm.WeekNumber = date.WeekNumber;
                arvm.SearchType = SearchType.StartDateOnly;
                List<ActiveRegistrationViewModel> arvmList = new List<ActiveRegistrationViewModel>();
                arvmList.Add(arvm);
                return View(arvmList);
            }
            else if (searchDateEnd != null)
            {
                DateTime wednesday = GetNearestPastCalendarDate(calendarDates, GetDate(searchDateEnd)).DateStart;
                IEnumerable<Registration> registrations = _db.Registrations.Where(r => r.Status == "Active");
                CalendarDate date = _db.CalendarDates.Where(cd => cd.DateStart.ToShortDateString() == wednesday.ToShortDateString()).FirstOrDefault();
                Draw draw = _db.Draws.Where(d => d.CalendarDateID == date.Id).FirstOrDefault();
                int regCount = (from reg in registrations
                                where reg.DateCreated <= date.DateStart
                                select reg).Count();
                ActiveRegistrationViewModel arvm = new ActiveRegistrationViewModel();
                arvm.Total = regCount;
                arvm.TotalPlayed = (draw != null) ? draw.StickerCount : 0;
                arvm.Week = date.DateStart.ToShortDateString();
                arvm.WeekNumber = date.WeekNumber;
                arvm.SearchType = SearchType.EndDateOnly;
                List<ActiveRegistrationViewModel> arvmList = new List<ActiveRegistrationViewModel>();
                arvmList.Add(arvm);
                return View(arvmList);
            }
            return View();
        }
        private ActiveRegistrationViewModel GetActiveRegistrationsViewModel(CalendarDate calendarDate, List<Draw> draws, List<Registration> registrations, SearchType searchType)
        {
            ActiveRegistrationViewModel allRegistrationsViewModel = new ActiveRegistrationViewModel();
            int activeRegistrations = (from reg in registrations
                                       where reg.DateCreated <= calendarDate.DateStart && reg.Status == "Active"
                                       select reg).Count();
            Draw currentDraw = (from draw in draws
                                where draw.CalendarDateID == calendarDate.Id
                                select draw).FirstOrDefault();
            int playedRegistrations = currentDraw == null ? 0 : currentDraw.StickerCount;
            allRegistrationsViewModel.Total = activeRegistrations;
            allRegistrationsViewModel.TotalPlayed = currentDraw != null ? currentDraw.StickerCount : 0;
            allRegistrationsViewModel.Week = calendarDate.DateStart.ToShortDateString();
            allRegistrationsViewModel.WeekNumber = calendarDate.WeekNumber;
            allRegistrationsViewModel.SearchType = searchType;
            return allRegistrationsViewModel;
        }
        private DateTime GetDate(string dateString)
        {
            if (string.IsNullOrEmpty(dateString)) return GetLastWednesday();
            string[] parts = null;
            if (dateString.Contains("-"))
            {
                parts = dateString.Split('-');
            }
            else if (dateString.Contains("/"))
            {
                parts = dateString.Split('/');
            }
            int year = int.Parse(parts[0]);
            int month = 0;
            if (parts[1].StartsWith('0')) month = int.Parse(parts[1].Substring(1));
            else month = int.Parse(parts[1]);
            int day = 0;
            if (parts[2].StartsWith('0')) day = int.Parse(parts[2].Substring(1));
            else day = int.Parse(parts[2]);
            return new DateTime(year, month, day);
        }
        private DateTime GetLastWednesday()
        {
            DateTime lastWednesday = DateTime.Now;
            DateTime now = DateTime.Now;
            if (now.DayOfWeek == DayOfWeek.Wednesday)
            {
                lastWednesday = now;
            }
            else
            {
                // go back a week
                now = now.Subtract(TimeSpan.FromDays(7.0));
                for (int i = 0; i < 7; i++)
                {
                    if (now.DayOfWeek == DayOfWeek.Wednesday)
                    {
                        break;
                    }
                    else
                    {
                        // keep adding days until you find Wednesday
                        now = now.AddDays(1.0);
                    }
                }
                lastWednesday = now;
            }
            return lastWednesday;
        }
    }
}