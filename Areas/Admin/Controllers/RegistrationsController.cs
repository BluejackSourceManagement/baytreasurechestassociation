﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BayTreasureChestAssociation.Data;
using BayTreasureChestAssociation.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BayTreasureChestAssociation.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Administrator,Editor,ReadOnly")]
    public class RegistrationsController : Controller
    {
        private readonly ApplicationDbContext _db;

        public RegistrationsController(ApplicationDbContext db)
        {
            _db = db;
        }

        // GET Action Method for Swap Player
        [Authorize(Roles = "Administrator,Editor")]
        public IActionResult SwapRegistrationPlayer(int? id)
        {
            if (id == null) return NotFound();
            Registration reg = _db.Registrations.Find(id);
            if (reg == null) return NotFound();
            List<Player> players = _db.Players.ToList();
            //List<Registration> regs = new List<Registration>();
            //regs.Add(reg);
            Tuple<List<Player>, DBSearchViewModel> regAndPlayers = new Tuple<List<Player>, DBSearchViewModel>(new List<Player>(), new DBSearchViewModel());
            if (SingletonDataObject.data.Keys.Contains("RegistrationIdForSwap")) SingletonDataObject.data.Remove("RegistrationIdForSwap");
            SingletonDataObject.data["RegistrationIdForSwap"] = reg.Id;
            if (SingletonDataObject.data.Keys.Contains("Communities")) SingletonDataObject.data.Remove("Communities");
            SingletonDataObject.data.Add("Communities", _db.Communities.OrderBy(c => c.Name));
            return View(regAndPlayers);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator,Editor")]
        public IActionResult SwapRegistrationPlayer(
            string firstName = null,
            string middleName = null,
            string lastName = null,
            string phone = null,
            string email = null,
            string province = null,
            string address = null,
            string postalCode = null,
            string country = null,
            string community = null,
            string comments = null)
        {
            #region Bunch of if statements
            IEnumerable<Player> players = _db.Players;
            if (firstName != null) players = players.Where(p =>
            {
                if (p.FirstName != null)
                    return p.FirstName.ToLower().Contains(firstName.ToLower());
                else
                    return false;
            });
            if (middleName != null) players = players.Where(p =>
            {
                if (p.MiddleName != null)
                    return p.MiddleName.ToLower().Contains(middleName.ToLower());
                else
                    return false;
            });
            if (lastName != null) players = players.Where(p =>
            {
                if (p.LastName != null)
                    return p.LastName.ToLower().Contains(lastName.ToLower());
                else
                    return false;
            });
            if (phone != null) players = players.Where(p =>
            {
                if (p.Phone != null)
                    return p.Phone.ToLower().Contains(phone.ToLower());
                else
                    return false;
            });
            if (email != null) players = players.Where(p =>
            {
                if (p.Email != null)
                    return p.Email.ToLower().Contains(email.ToLower());
                else
                    return false;
            });
            if (province != null && province != " ") players = players.Where(p =>
            {
                if (p.Province != null)
                    return p.Province.ToLower().Contains(province.ToLower());
                else
                    return false;
            });
            if (address != null) players = players.Where(p =>
            {
                if (p.Address != null)
                    return p.Address.ToLower().Contains(address.ToLower());
                else
                    return false;
            });
            if (postalCode != null) players = players.Where(p =>
            {
                if (p.Postal != null)
                    return p.Postal.ToLower().Contains(postalCode.ToLower());
                else
                    return false;
            });
            if (community != null && community != " ") players = players.Where(p =>
            {
                if (p.Community != null)
                    return p.Community.ToLower().Contains(community.ToLower());
                else
                    return false;
            });
            if (country != null && country != " ") players = players.Where(p =>
            {
                if (p.Country != null)
                    return p.Country.ToLower().Contains(country.ToLower());
                else
                    return false;
            });
            if (comments != null) players = players.Where(p =>
            {
                if (p.Comments != null)
                    return p.Comments.ToLower().Contains(comments.ToLower());
                else
                    return false;
            });
            #endregion
            if (SingletonDataObject.data.Keys.Contains("Communities")) SingletonDataObject.data.Remove("Communities");
            SingletonDataObject.data.Add("Communities", _db.Communities.OrderBy(c => c.Name));
            DBSearchViewModel dBSearchViewModel = new DBSearchViewModel
            {
                FirstName = firstName,
                MiddleName = middleName,
                LastName = lastName,
                Phone = phone,
                Email = email,
                PlayerComments = comments,
                Address = address,
                Province = province,
                PostalCode = postalCode,
                Community = community
            };
            if (firstName == null && middleName == null && lastName == null && phone == null &&
                email == null && province == null && address == null && postalCode == null &&
                comments == null && community == null)
            {
                Tuple<List<Player>, DBSearchViewModel> regAndPlayers = new Tuple<List<Player>, DBSearchViewModel>(new List<Player>(), dBSearchViewModel);
                return View(regAndPlayers);
            }
            else
            {
                List<Player> filteredPlayers = players.ToList();
                Tuple<List<Player>, DBSearchViewModel> regAndPlayers = new Tuple<List<Player>, DBSearchViewModel>(players.ToList(), dBSearchViewModel);
                return View(regAndPlayers);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator,Editor")]
        public async Task<IActionResult> SwapPlayerOut(int id, int regId)
        {
            Registration reg = _db.Registrations.Find(regId);
            Player player = _db.Players.Find(id);
            reg.Player = player;
            reg.PlayerId = player.Id;
            _db.Registrations.Update(reg);
            if (player.Registrations == null) player.Registrations = new List<Registration>();
            player.Registrations.Add(reg);
            await _db.SaveChangesAsync();
            // if the new registration number is greater than 0 then we can 
            // remove any negative reg numbers associated with the player.
            if (reg.RegNumber > 0) ScrubNegativeRegistrationNumbers(id);
            if (SingletonDataObject.data.Keys.Contains("SwappedRegistration")) SingletonDataObject.data.Remove("SwappedRegistration");
            SingletonDataObject.data["SwappedRegistration"] = new PlayerRegistrationViewModel()
            {
                Player = player,
                Registration = reg
            };
            return RedirectToAction("/ShowDetailsForPlayerById/" + player.Id);
        }


        // GET ModifyPlayerById Action Method
        [Authorize(Roles = "Administrator,Editor")]
        public IActionResult ModifyPlayerById(int? id)
        {
            if (id == null) return NotFound();
            Player player = _db.Players.Find(id);
            if (player == null) return NotFound();
            if (SingletonDataObject.data.Keys.Contains("Communities")) SingletonDataObject.data.Remove("Communities");
            SingletonDataObject.data["Communities"] = _db.Communities;
            return View(player);
        }

        // POST ModifyPlayerById Action Method
        [HttpPost, ActionName("ModifyPlayerById")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator,Editor")]
        public async Task<IActionResult> ModifyPlayerById(int? id, Player player)
        {
            if (id != player.Id) return NotFound();
            if (player == null) return NotFound();
            Player playerInDb = _db.Players.Find(id);
            playerInDb.Address = player.Address;
            playerInDb.Comments = player.Comments;
            playerInDb.Community = player.Community;
            playerInDb.Country = player.Country;
            playerInDb.Email = player.Email;
            playerInDb.FirstName = player.FirstName;
            playerInDb.LastName = player.LastName;
            playerInDb.LastUpdated = DateTime.Now;
            playerInDb.MiddleName = player.MiddleName;
            playerInDb.Phone = player.Phone;
            playerInDb.RequiresContact = player.RequiresContact;
            if (string.IsNullOrEmpty(player.Postal)) playerInDb.Postal = null;
            else playerInDb.Postal = player.Postal.ToUpper();
            playerInDb.Province = player.Province;
            _db.Players.Update(playerInDb);
            await _db.SaveChangesAsync();
            return RedirectToAction("/ShowDetailsForPlayerById/" + id);
        }
        // GET ModifyRegistrationById Action Method
        [Authorize(Roles = "Administrator,Editor")]
        public IActionResult ModifyRegistrationById(int? id)
        {
            if (id == null) return NotFound();
            Registration reg = _db.Registrations.Find(id);
            if (reg == null) return NotFound();
            return View(reg);
        }

        // POST ModifyRegistrationById Action Method
        [HttpPost, ActionName("ModifyRegistrationById")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator,Editor")]
        public async Task<IActionResult> ModifyRegistrationById(int id, Registration registration)
        {
            if (ModelState.IsValid)
            {
                Registration reg = await _db.Registrations.FindAsync(id);
                reg.Comments = registration.Comments;
                reg.DateCreated = registration.DateCreated;
                reg.DateRetired = registration.DateRetired;
                reg.RegNumber = registration.RegNumber;
                reg.Status = registration.Status;
                _db.Registrations.Update(reg);
                await _db.SaveChangesAsync();
                if (reg.RegNumber > 0)
                {
                    ScrubNegativeRegistrationNumbers(reg.PlayerId);
                }
                return RedirectToAction("/ShowDetailsForPlayerById/" + reg.PlayerId);
            }
            return View(registration);
        }

        /// <summary>
        /// LiNQ to Objects used to filter both lists and return related players only.
        /// </summary>
        /// <param name="registrations"></param>
        /// <param name="players"></param>
        /// <returns></returns>
        List<Player> FilterPlayers(IEnumerable<Registration> registrations, IEnumerable<Player> players)
        {
            IEnumerable<Player> filteredPlayers = (from player in players
                                                   join regs in registrations on player.Id equals regs.PlayerId
                                                   select player).Distinct();
            return filteredPlayers.ToList();
        }

        private DateTime GetDate(string dateString)
        {
            if (dateString != null)
            {
                int year, month, day;
                string[] parts = null;
                if (dateString.Contains("-"))
                {
                    parts = dateString.Split('-');
                }
                else if (dateString.Contains("/"))
                {
                    parts = dateString.Split('/');
                }
                year = Convert.ToInt32(parts[0]);
                if (parts[1].StartsWith('0'))
                    parts[1] = parts[1].Remove(0, 1);
                if (parts[2].StartsWith('0'))
                    parts[2] = parts[2].Remove(0, 1);
                month = Convert.ToInt32(parts[1]);
                day = Convert.ToInt32(parts[2]);
                return new DateTime(year, month, day);
            }
            return DateTime.Now;
        }
        // GET Search Action Method
        public IActionResult Index(
            string reg = null,
            string firstName = null,
            string middleName = null,
            string lastName = null,
            string phone = null,
            string email = null,
            string address = null,
            string province = null,
            string postalCode = null,
            string community = null,
            string comments = null,
            string regComments = null,
            string dateRegistered = null,
            string dateRetired = null,
            string regDateEnd = null,
            string retDateEnd = null,
            string country = null,
            string status = null,
            string requiresContact = null,
            string lastUpdated = null,
            string lastUpdatedEnd = null)
        {
            if (SingletonDataObject.data.Keys.Contains("Communities")) SingletonDataObject.data.Remove("Communities");
            SingletonDataObject.data["Communities"] = _db.Communities;
            QueryString queryString = HttpContext.Request.QueryString;
            if (SingletonDataObject.data.Keys.Contains("QueryString")) SingletonDataObject.data.Remove("QueryString");
            SingletonDataObject.data["QueryString"] = queryString;
            IEnumerable<Player> players = _db.Players;
            IEnumerable<Registration> registrations = _db.Registrations;

            if (reg != null)
            {
                IEnumerable<Player> filteredPlayers = (from player in players
                                                       join regs in registrations on player.Id equals regs.PlayerId
                                                       select player).Distinct();
                int regNum = Convert.ToInt32(reg);
                registrations = registrations.Where(r => r.RegNumber == regNum);
                //registrations = from regin in registrations
                //                where regin.RegNumber==regNum
                //                select regin;
            }
            if (regComments != null)
            {
                registrations = registrations.Where(r => r.Comments != null && r.Comments.ToLower().Contains(regComments.ToLower()));
            }
            if (dateRegistered != null && regDateEnd != null)
            {
                registrations = registrations.Where(r => (r.DateCreated >= GetDate(dateRegistered) && r.DateCreated <= GetDate(regDateEnd)));
            }
            else if (dateRegistered != null)
            {
                registrations = registrations.Where(r => r.DateCreated.ToShortDateString() == dateRegistered);
            }
            else if (regDateEnd != null)
            {
                registrations = registrations.Where(r => r.DateCreated.ToShortDateString() == regDateEnd);
            }
            if (dateRetired != null && retDateEnd != null)
            {
                registrations = registrations.Where(r => (r.DateRetired >= GetDate(dateRetired) && r.DateRetired <= GetDate(retDateEnd)));
            }
            else if (dateRetired != null)
            {
                registrations = registrations.Where(r => (r.DateRetired != null && ((DateTime)r.DateRetired).ToShortDateString() == dateRetired));
            }
            else if (retDateEnd != null)
            {
                registrations = registrations.Where(r => (r.DateRetired != null && ((DateTime)r.DateRetired).ToShortDateString() == retDateEnd));
            }

            if (status != null && status != " ")
            {
                registrations = registrations.Where(r => r.Status != null ? r.Status.ToLower() == status.ToLower() : false);
            }
            if (firstName != null) players = players.Where(p =>
            {
                if (p.FirstName != null)
                    return p.FirstName.ToLower().Contains(firstName.ToLower());
                else return false;
            });
            if (middleName != null) players = players.Where(p =>
            {
                if (p.MiddleName != null)
                    return p.MiddleName.ToLower().Contains(middleName.ToLower());
                else return false;
            });
            if (lastName != null) players = players.Where(p =>
            {
                if (p.LastName != null)
                    return p.LastName.ToLower().Contains(lastName.ToLower());
                else return false;
            });
            if (phone != null) players = players.Where(p =>
            {
                if (p.Phone != null)
                    return p.Phone.ToLower().Contains(phone.ToLower());
                else return false;
            });
            if (email != null) players = players.Where(p =>
            {
                if (p.Email != null)
                    return p.Email.ToLower().Contains(email.ToLower());
                else return false;
            });
            if (address != null) players = players.Where(p =>
            {
                if (p.Address != null)
                    return p.Address.ToLower().Contains(address.ToLower());
                else return false;
            });
            if (province != null && province != " ") players = players.Where(p =>
            {
                if (p.Province != null)
                    return p.Province.ToLower().Contains(province.ToLower());
                else return false;
            });
            if (country != null && country != " ") players = players.Where(p =>
            {
                if (p.Country != null)
                    return p.Country.ToLower().Contains(country.ToLower());
                else return false;
            });

            if (postalCode != null) players = players.Where(p =>
            {
                if (p.Postal != null)
                    return p.Postal.ToLower().Contains(postalCode.ToLower());
                else return false;
            });
            if (community != null && community != " ") players = players.Where(p =>
            {
                if (p.Community != null)
                    return p.Community.ToLower().Contains(community.ToLower());
                else return false;
            });
            if (comments != null) players = players.Where(p =>
            {
                if (p.Comments != null)
                    return p.Comments.ToLower().Contains(comments.ToLower());
                else return false;
            });
            if (requiresContact == "Yes")
                players = players.Where(p => p.RequiresContact);
            if (requiresContact == "No")
                players = players.Where(p => !p.RequiresContact);

            if (lastUpdated != null && lastUpdatedEnd != null)
            {
                players = players.Where(p => p.LastUpdated >= GetDate(lastUpdated) && p.LastUpdated <= GetDate(lastUpdatedEnd));
            }
            else if (lastUpdated != null)
            {
                players = players.Where(p => p.LastUpdated.ToShortDateString() == lastUpdated);
            }
            else if (lastUpdatedEnd != null)
            {
                players = players.Where(p => p.LastUpdated.ToShortDateString() == lastUpdatedEnd);
            }
            if (reg == null &&
             firstName == null &&
             middleName == null &&
             lastName == null &&
             phone == null &&
             email == null &&
             address == null &&
             province == null &&
             postalCode == null &&
             community == null &&
             comments == null &&
             regComments == null &&
             dateRegistered == null &&
             dateRetired == null &&
             regDateEnd == null &&
             retDateEnd == null &&
             country == null &&
             status == null &&
             requiresContact == null &&
             lastUpdated == null &&
             lastUpdatedEnd == null)
            {
                DBSearchViewModel dBSearchViewModel = new DBSearchViewModel()
                {
                    Address = null,
                    Community = null,
                    Country = null,
                    DateRegistered = null,
                    DateRegisteredEnd = null,
                    DateRetired = null,
                    DateRetiredEnd = null,
                    Email = null,
                    FirstName = null,
                    LastName = null,
                    LastUpdated = null,
                    LastUpdatedEnd = null,
                    MiddleName = null,
                    Phone = null,
                    PlayerComments = null,
                    PostalCode = null,
                    Province = null,
                    RegistrationComments = null,
                    RegistrationNumber = null,
                    RequiresContact = null,
                    Status = null
                };
                return View(new Tuple<List<Registration>, List<Player>, DBSearchViewModel>(new List<Registration>(), new List<Player>(), dBSearchViewModel));
            }
            else
            {
                DBSearchViewModel dBSearchViewModel = new DBSearchViewModel();
                dBSearchViewModel.Address = address;
                dBSearchViewModel.Community = community;
                dBSearchViewModel.Country = country;
                dBSearchViewModel.DateRegistered = dateRegistered;
                dBSearchViewModel.DateRegisteredEnd = regDateEnd;
                dBSearchViewModel.DateRetired = dateRetired;
                dBSearchViewModel.DateRetiredEnd = retDateEnd;
                dBSearchViewModel.Email = email;
                dBSearchViewModel.FirstName = firstName;
                dBSearchViewModel.LastName = lastName;
                dBSearchViewModel.LastUpdated = lastUpdated;
                dBSearchViewModel.LastUpdatedEnd = lastUpdatedEnd;
                dBSearchViewModel.MiddleName = middleName;
                dBSearchViewModel.Phone = phone;
                dBSearchViewModel.PlayerComments = comments;
                dBSearchViewModel.PostalCode = postalCode;
                dBSearchViewModel.Province = province;
                dBSearchViewModel.RegistrationComments = regComments;
                dBSearchViewModel.RegistrationNumber = reg;
                dBSearchViewModel.RequiresContact = requiresContact;
                dBSearchViewModel.Status = status;
                List<Registration> results = new List<Registration>();
                // need to get the filtered registrations for each filtered player collected and then return them
                players = FilterPlayers(registrations, players);
                if (players != null && players.Count() > 0)
                {
                    IEnumerable<Registration> filteredRegs = (from regn in registrations
                                                              join player in players on regn.PlayerId equals player.Id
                                                              select regn).Distinct();
                    results = filteredRegs.ToList();
                }
                else
                {
                    Player player = new Player() { FirstName = "No results." };
                    List<Player> fakes = new List<Player>();
                    fakes.Add(player);
                    players = fakes;
                }

                Tuple<List<Registration>, List<Player>, DBSearchViewModel> _regsAndPlayers =
                    new Tuple<List<Registration>, List<Player>, DBSearchViewModel>(results, players.ToList(), dBSearchViewModel);
                return View(_regsAndPlayers);
            }
        }

        public IActionResult ShowDetailsForPlayerById(int? id)
        {
            if (id == null) return NotFound();
            Player player = _db.Players.Find(id);
            List<Registration> regs = _db.Registrations.Where(r => r.PlayerId == id).ToList();
            Tuple<Player, List<Registration>> tuple = new Tuple<Player, List<Registration>>(player, regs);
            return View(tuple);
        }

        [Authorize(Roles = "Administrator,Editor")]
        public IActionResult AddRegistrationToPlayerById(int? id)
        {
            if (id == null) return NotFound();
            Player player = _db.Players.Find(id);
            Registration reg = new Registration();
            PlayerRegistrationViewModel prvm = new PlayerRegistrationViewModel { Player = player, Registration = reg };
            return View(prvm);
        }

        private void ScrubNegativeRegistrationNumbers(int playerId)
        {
            IEnumerable<Registration> registrations = _db.Registrations.Where(r => r.PlayerId == playerId && r.RegNumber <= 0);
            if (registrations != null && registrations.Count() > 0)
            {
                foreach (Registration registration in registrations)
                {
                    _db.Registrations.Remove(_db.Registrations.Find(registration.Id));
                }
                _db.SaveChanges();
            }
        }

        [HttpPost, ActionName("AddRegistrationToPlayerById")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator,Editor")]
        public async Task<IActionResult> AddRegistrationToPlayerById(int id, PlayerRegistrationViewModel prvm)
        {
            if (ModelState.IsValid)
            {
                Player player = _db.Players.Find(id);
                Registration reg = new Registration();
                //reg.HasPlayed = true;
                reg.Player = player;
                reg.PlayerId = id;
                reg.RegNumber = prvm.Registration.RegNumber;
                reg.DateCreated = prvm.Registration.DateCreated;
                reg.DateRetired = null;
                if (reg.RegNumber < 1) reg.Status = "Other";
                else reg.Status = "Active";
                reg.Comments = prvm.Registration.Comments;
                if (player.Registrations == null) player.Registrations = new List<Registration>();
                player.Registrations.Add(reg);
                _db.Registrations.Add(reg);
                await _db.SaveChangesAsync();
                if (reg.RegNumber > 0) ScrubNegativeRegistrationNumbers(id);
                if (SingletonDataObject.data.Keys.Contains("LastDateRegistered")) SingletonDataObject.data.Remove("LastDateRegistered");
                SingletonDataObject.data["LastDateRegistered"] = reg.DateCreated;
                if (SingletonDataObject.data.Keys.Contains("LastRegistration")) SingletonDataObject.data.Remove("LastRegistration");
                SingletonDataObject.data["LastRegistration"] = reg;
                return View(prvm);
            }
            return View();
        }

        [Authorize(Roles = "Administrator,Editor")]
        public IActionResult AddRegistrationToExistingPlayer(PlayerRegistrationViewModel prvm)
        {
            PlayerRegistrationViewModel myPrvm = SingletonDataObject.data["CurrentPlayerViewModel"] as PlayerRegistrationViewModel;
            return View(myPrvm);
        }

        [HttpPost, ActionName("AddRegistrationToExistingPlayer")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator,Editor")]
        public async Task<IActionResult> AddRegistrationToExistingPlayerNow(PlayerRegistrationViewModel plvm)
        {
            if (ModelState.IsValid)
            {
                // tried this with a Linq Where query below 
                // but it fails for some reason
                Player newPlayer = null;
                foreach (Player player in _db.Players)
                {
                    if (player.Equals(plvm.Player))
                    {
                        newPlayer = player;
                        break;
                    }
                }
                //Player newPlayer = _db.Players.Where(p => p.Equals(plvm.Player)).FirstOrDefault();
                plvm.Registration.PlayerId = newPlayer.Id;
                Registration reg = new Registration();
                //reg.HasPlayed = true;
                reg.Player = newPlayer;
                reg.PlayerId = newPlayer.Id;
                reg.RegNumber = plvm.Registration.RegNumber;
                if (reg.RegNumber < 1) reg.Status = "Other";
                else reg.Status = "Active";
                reg.DateCreated = plvm.Registration.DateCreated;
                reg.DateRetired = null;
                reg.Comments = plvm.Registration.Comments;
                _db.Registrations.Add(reg);
                await _db.SaveChangesAsync();
                if (reg.RegNumber > 0) ScrubNegativeRegistrationNumbers(newPlayer.Id);
                if (SingletonDataObject.data.Keys.Contains("LastDateRegistered")) SingletonDataObject.data.Remove("LastDateRegistered");
                SingletonDataObject.data["LastDateRegistered"] = reg.DateCreated;
                if (SingletonDataObject.data.Keys.Contains("LastRegistration")) SingletonDataObject.data.Remove("LastRegistration");
                SingletonDataObject.data["LastRegistration"] = reg;
                return RedirectToAction(nameof(AddRegistrationToExistingPlayer));
            }
            return View();
        }

        // Get Action Method 
        [Authorize(Roles = "Administrator,Editor")]
        public IActionResult AddRegistrationToPlayer()
        {
            if (SingletonDataObject.data.Keys.Contains("Communities")) SingletonDataObject.data.Remove("Communities");
            SingletonDataObject.data.Add("Communities", _db.Communities.ToList().OrderBy(item => item.Name));
            return View();
        }

        [HttpPost, ActionName("AddRegistrationToPlayer")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator,Editor")]
        public async Task<IActionResult> AddRegistrationToPlayerNow(PlayerRegistrationViewModel prvm)
        {
            if (ModelState.IsValid)
            {
                DateTime now = DateTime.Now;
                prvm.Player.LastUpdated = now;
                if (prvm.Player.Postal != null)
                    prvm.Player.Postal = prvm.Player.Postal.ToUpper();
                _db.Add(prvm.Player);
                await _db.SaveChangesAsync();
                Player newPlayer = _db.Players.Where(p => p.Equals(prvm.Player)).FirstOrDefault();
                if (newPlayer == null) return View();
                Registration reg = new Registration();
                //reg.HasPlayed = true;
                reg.Player = newPlayer;
                reg.PlayerId = newPlayer.Id;
                reg.RegNumber = prvm.Registration.RegNumber;
                if (reg.RegNumber < 1) reg.Status = "Other";
                else reg.Status = "Active";
                reg.DateCreated = prvm.Registration.DateCreated;
                // use a default date for retired
                reg.DateRetired = null;
                reg.Comments = prvm.Registration.Comments;
                _db.Registrations.Add(reg);
                await _db.SaveChangesAsync();
                if (reg.RegNumber > 0) ScrubNegativeRegistrationNumbers(reg.PlayerId);
                PlayerRegistrationViewModel newPrvm = new PlayerRegistrationViewModel();
                newPrvm.Player = newPlayer;
                newPrvm.Registration = reg;
                if (SingletonDataObject.data.Keys.Contains("CurrentPlayerViewModel")) SingletonDataObject.data.Remove("CurrentPlayerViewModel");
                SingletonDataObject.data.Add("CurrentPlayerViewModel", newPrvm);
                if (SingletonDataObject.data.Keys.Contains("LastDateRegistered")) SingletonDataObject.data.Remove("LastDateRegistered");
                SingletonDataObject.data["LastDateRegistered"] = reg.DateCreated;
                if (SingletonDataObject.data.Keys.Contains("LastRegistration")) SingletonDataObject.data.Remove("LastRegistration");
                SingletonDataObject.data["LastRegistration"] = reg;
                return RedirectToAction(nameof(AddRegistrationToExistingPlayer));
            }

            return View();
        }

        public JsonResult IsDuplicateRegNum(string RegNumber)
        {

            // can't validate
            if (RegNumber == "NaN") return Json(true);
            if (Convert.ToInt32(RegNumber) <= 0) return Json(false); // duplicate negative numbers allowed.
            /// Note: The parameter name must match the spelling of the actual 
            /// property in the registration object 
            Registration validateRegNum = _db.Registrations.FirstOrDefault(reg => reg.RegNumber == Convert.ToInt32(RegNumber));
            if (validateRegNum != null) return Json(true);
            return Json(false);
        }

        public JsonResult GetRegistrationComments(int regId)
        {
            Registration reg = _db.Registrations.Find(regId);
            if (reg == null) return Json(null);
            return Json(reg.Comments);
        }

        public JsonResult SetRegistrationComments(int regId, string comments)
        {
            Registration reg = _db.Registrations.Find(regId);
            if (reg == null) return Json("failed");
            reg.Comments = comments;
            _db.Update(reg);
            _db.SaveChanges();
            return Json("success");
        }

    }
}