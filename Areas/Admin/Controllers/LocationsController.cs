﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BayTreasureChestAssociation.Data;
using BayTreasureChestAssociation.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BayTreasureChestAssociation.Areas.Admin.Controllers
{
    [Authorize(Roles = "Administrator,Editor")]
    [Area("Admin")]
    public class LocationsController : Controller
    {
        private readonly ApplicationDbContext _db;
        //public const Tuple<Location, IEnumerable<Community>> tuple = new Tuple<Location, IEnumerable<Community>>(new Location(), new List<Community>());
        public LocationsController(ApplicationDbContext db)
        {
            _db = db;
            /*tuple = new Tuple<Location, IEnumerable<Community>>(new Location(), _db.Communities.ToList());*/
        }
        public IActionResult Index()
        {
            Tuple<IEnumerable<Location>, IEnumerable<Community>> tuple = new Tuple<IEnumerable<Location>, IEnumerable<Community>>(_db.Locations.ToList(), _db.Communities.ToList());
            return View(tuple);
        }

        public IActionResult Create()
        {
            return View();
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null) return NotFound();
            Location location = await _db.Locations.FindAsync(id);
            if (location == null) return NotFound();
            return View(location);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, Location location)
        {
            if (id != location.Id) return NotFound();
            if (ModelState.IsValid)
            {
                _db.Update(location);
                await _db.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(location);
        }

        public JsonResult Delete(int id)
        {
            Location location = _db.Locations.Find(id);
            _db.Locations.Remove(location);
            _db.SaveChanges();
            return Json("Successfully deleted");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Location location)
        {
            if (ModelState.IsValid)
            {
                _db.Add(location);
                await _db.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            if (SingletonDataObject.data.Keys.Contains("Communities"))
            {
                SingletonDataObject.data.Remove("Communities");
            }
            SingletonDataObject.data["Communities"] = _db.Communities;
            return View(location); // invalid model so stay on current page
        }

        public JsonResult IsDuplicatePlayLocation(string location, string community)
        {
            Location location1 = _db.Locations.Where(loc => loc.PlayLocation == location && loc.Community == community).FirstOrDefault();
            return Json(location1 != null);
        }
    }
}