﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BayTreasureChestAssociation.Data;
using BayTreasureChestAssociation.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BayTreasureChestAssociation.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Administrator,Editor,ReadOnly")]
    public class ApplicationSettingsController : Controller
    {
        private readonly ApplicationDbContext _db;
        public ApplicationSettingsController(ApplicationDbContext db)
        {
            _db = db;
        }
        public IActionResult Index()
        {
            return View(_db.ApplicationSettings.FirstOrDefault());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Update(int id, ApplicationSettings settings)
        {
            ApplicationSettings dbSettings = _db.ApplicationSettings.Find(id);
            dbSettings.NSAGLicense = settings.NSAGLicense;
            _db.Update(dbSettings);
            _db.SaveChanges();
            return View(nameof(Index), _db.ApplicationSettings.FirstOrDefault());

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(ApplicationSettings settings)
        {
            _db.ApplicationSettings.Add(settings);
            _db.SaveChanges();
            return View(nameof(Index), _db.ApplicationSettings.FirstOrDefault());
        }

        public JsonResult GetNSAGLicense()
        {
            ApplicationSettings settings = _db.ApplicationSettings.FirstOrDefault();
            if (settings == null)
                settings = new ApplicationSettings()
                {
                    NSAGLicense = "None available."
                };
            return Json(settings);
        }
    }
}