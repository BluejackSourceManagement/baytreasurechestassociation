﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BayTreasureChestAssociation.Data;
using BayTreasureChestAssociation.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BayTreasureChestAssociation.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Administrator,Editor,ReadOnly")]
    public class NonWinningDrawsController : Controller
    {
        private readonly ApplicationDbContext _db;
        public NonWinningDrawsController(ApplicationDbContext db)
        {
            _db = db;
        }

        public IActionResult Index()
        {
            return View(_db.NonWinningDraws);
        }

        [Authorize(Roles = "Administrator,Editor")]
        public IActionResult Create()
        {
            DateTime now = DateTime.Now;
            if (now.DayOfWeek != DayOfWeek.Wednesday)
            {
                // go back a week
                now = now.Subtract(TimeSpan.FromDays(7.0));
                for (int i = 0; i < 7; i++)
                {
                    if (now.DayOfWeek == DayOfWeek.Wednesday)
                    {
                        break;
                    }
                    else
                    {
                        // keep adding days until you find Wednesday
                        now = now.AddDays(1.0);
                    }
                }
            }
            List<CalendarDate> dates = _db.CalendarDates.ToList();
            int weekNumber = (from date in dates
                              where date.DateStart.ToShortDateString() == now.ToShortDateString()
                              select date).First().WeekNumber;
            NonWinningDraw nonWinningDraw = new NonWinningDraw() { DrawDate = now, RegistrationNumber = 0, WeekNumber = weekNumber, Reason = string.Empty };
            return View(nonWinningDraw);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator,Editor")]
        public async Task<IActionResult> Create(NonWinningDraw nonWinningDraw)
        {
            if (ModelState.IsValid)
            {
                _db.Add(nonWinningDraw);
                await _db.SaveChangesAsync();
                NonWinningDraw dummy = new NonWinningDraw()
                {
                    RegistrationNumber = 0,
                    WeekNumber = nonWinningDraw.WeekNumber,
                    DrawDate = nonWinningDraw.DrawDate,
                    Reason = ""
                };
                return View(dummy);
            }
            else
            {
                return View(nonWinningDraw);
            }
        }

        [Authorize(Roles = "Administrator,Editor")]
        public JsonResult Delete(int id)
        {
            NonWinningDraw nonWinningDraw = _db.NonWinningDraws.Find(id);
            _db.NonWinningDraws.Remove(nonWinningDraw);
            _db.SaveChanges();
            return Json("Successfully deleted");
        }

        public JsonResult GetCalendarWeekDate(int weekNumber)
        {
            CalendarDate calendarDate = _db.CalendarDates.Where(cd => cd.WeekNumber == weekNumber).FirstOrDefault();
            return Json(calendarDate.DateStart.ToShortDateString());
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator,Editor")]
        public async Task<IActionResult> Edit(int id, NonWinningDraw nonWinningDraw)
        {
            if (id != nonWinningDraw.Id) return NotFound();
            if (ModelState.IsValid)
            {
                _db.Update(nonWinningDraw);
                await _db.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(nonWinningDraw);
        }
        [Authorize(Roles = "Administrator,Editor")]
        public IActionResult Edit(int? id)
        {
            if (id == null) return RedirectToAction(nameof(Index));
            NonWinningDraw draw = _db.NonWinningDraws.Find(id);
            return View(draw);
        }
    }
}