﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BayTreasureChestAssociation.Areas.Identity.Pages.Account;
using BayTreasureChestAssociation.Data;
using BayTreasureChestAssociation.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace BayTreasureChestAssociation.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Administrator")]
    public class UsersController : Controller
    {
        private readonly ApplicationDbContext _db;
        private readonly UserManager<IdentityUser> _userManager;
        public UsersController(ApplicationDbContext db, UserManager<IdentityUser> userManager)
        {
            _db = db;
            _userManager = userManager;
        }
        public IActionResult Index()
        {
            return View(_db.ApplicationUsers.ToList());
        }

        public async Task<IActionResult> Edit(string id)
        {
            //if (id == null || id.Trim().Length == 0)
            //{
            //    return NotFound();
            //}
            //ApplicationUser user = await _db.ApplicationUsers.FindAsync(id);
            //if (user == null) return NotFound();
            //EditUserModel editUserModel = new EditUserModel()
            //{
            //    Email = user.Email,
            //    IsAdmin = user.IsAdmin,
            //    IsEditor = user.IsEditor,
            //    Name = user.Name,
            //    PhoneNumber = user.PhoneNumber
            //};

            //return View(user);
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, ApplicationUser user)
        {
            //if (id != user.Id)
            //{
            //    return NotFound();
            //}
            //if (ModelState.IsValid)
            //{
            //    ApplicationUser userfromDb = _db.ApplicationUsers.Where(u => u.Id == id).FirstOrDefault();
            //    userfromDb.Name = user.Name;
            //    userfromDb.PhoneNumber = user.PhoneNumber;
            //    await _db.SaveChangesAsync();
            //    return RedirectToAction(nameof(Index));
            //}
            return View(user);
        }
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null || id.Trim().Length == 0)
            {
                return NotFound();
            }
            ApplicationUser user = await _db.ApplicationUsers.FindAsync(id);
            if (user == null) return NotFound();
            return View(user);
        }
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeletePOST(string id)
        {
            ApplicationUser userfromDb = _db.ApplicationUsers.Where(u => u.Id == id).FirstOrDefault();
            if (userfromDb != null)
            {
                _db.Users.Remove(userfromDb);
                await _db.SaveChangesAsync();
            }
            return RedirectToAction(nameof(Index));
        }
    }
}