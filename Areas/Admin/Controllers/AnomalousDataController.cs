﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BayTreasureChestAssociation.Data;
using BayTreasureChestAssociation.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BayTreasureChestAssociation.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles ="Administrator,Editor,ReadOnly")]
    public class AnomalousDataController : Controller
    {
        // Continue to build Data anomaly search from Navigation with ability to search on a type of anomaly 
        // (missing email, missing phone number, orphan registration numbers) as well as ability to list all 
        // data issues within a given date range.
        // so, data anomolies are defined as 
        // a) players without email
        // b) players without phone
        // c) players without both
        // d) orphaned registrations?? 
        // e) date range search for both
        private readonly ApplicationDbContext _db;
        public AnomalousDataController(ApplicationDbContext db)
        {
            _db = db;
        }
        private DateTime GetDate(string dateString)
        {
            string[] parts = null;
            if (dateString.Contains("-"))
            {
                parts = dateString.Split('-');
            }
            else if (dateString.Contains("/"))
            {
                parts = dateString.Split('/');
            }
            int year = int.Parse(parts[0]);
            int month = 0;
            if (parts[1].StartsWith('0')) month = int.Parse(parts[1].Substring(1));
            else month = int.Parse(parts[1]);
            int day = 0;
            if (parts[2].StartsWith('0')) day = int.Parse(parts[2].Substring(1));
            else day = int.Parse(parts[2]);
            return new DateTime(year, month, day);
        }
        public IActionResult Index(
            string dateRegistered = null,
            string regDateEnd = null,
            string lastModified = null,
            string lastModifiedEnd = null)
        {
            if (SingletonDataObject.data.Keys.Contains("Communities"))
                SingletonDataObject.data.Remove("Communities");
            SingletonDataObject.data["Communities"] = _db.Communities.ToList();

            List<Player> players = _db.Players.Where(p => p.HasMissingInfo == true || (p.Phone == "000-000-0000" && string.IsNullOrEmpty(p.Email))).ToList();
            List<Registration> registrations = _db.Registrations.Where(r => r.PlayerId == 1).ToList();
            Tuple<List<Registration>, List<Player>> tuple = new Tuple<List<Registration>, List<Player>>(registrations, players);
            return View(tuple);
        }
    }
}