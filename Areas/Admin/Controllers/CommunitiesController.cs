﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BayTreasureChestAssociation.Data;
using BayTreasureChestAssociation.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BayTreasureChestAssociation.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Administrator,Editor")]
    public class CommunitiesController : Controller
    {
        private readonly ApplicationDbContext _db;
        public CommunitiesController(ApplicationDbContext db)
        {
            _db = db;
        }
        public IActionResult Index()
        {
            return View(_db.Communities.ToList().OrderBy(c=>c.Name));
        }

        // GET Create Action Method
        public IActionResult Create()
        {
            return View();
        }

        /// <summary>
        /// community parameter is provided by the form post 
        /// from the Create View.
        /// </summary>
        /// <param name="community"></param>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Community community)
        {
            if (ModelState.IsValid)
            {
                _db.Add(community);
                await _db.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(community); // invalid model so stay on current page
        }
        // GET Edit Action Method
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null) return NotFound();
            Community community = await _db.Communities.FindAsync(id);
            if (community == null) return NotFound();
            return View(community);
        }


        /// <summary>
        /// community parameter is provided by the form post 
        /// from the Edit View.
        /// </summary>
        /// <param name="community"></param>
        /// <param name="id"></param>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, Community community)
        {
            if (id != community.Id) return NotFound();
            if (ModelState.IsValid)
            {
                _db.Update(community);
                await _db.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(community); // invalid model so stay on current page
        }

        // GET Edit Action Method
        //public async Task<IActionResult> Delete(int? id)
        //{
        //    if (id == null) return NotFound();
        //    Community community = await _db.Communities.FindAsync(id);

        //    if (community == null) return NotFound();
        //    _db.Communities.Remove(community);
        //    await _db.SaveChangesAsync();
        //    return RedirectToAction(nameof(Index));
        //}
        public  JsonResult Delete(int id)
        {
            Community community = _db.Communities.Find(id);
            _db.Communities.Remove(community);
             _db.SaveChanges();
            return Json("Successfully deleted");
        }
        public JsonResult IsDuplicateCommunityName(string community)
        {
            Community com = _db.Communities.FirstOrDefault(c => c.Name == community);
            if (com != null) return Json(true);
            return Json(false);
        }
    }
}