﻿using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.Extensions.Options;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace BayTreasureChestAssociation.Services
{
    public class BTCAEmailSender : IEmailSender
    {
        private readonly IOptions< AppSettings> appSettings;
        public BTCAEmailSender(IOptions<AppSettings> appSettings)
        {
            this.appSettings = appSettings;
        }
        public Task SendEmailAsync(string email, string subject, string htmlMessage)
        {
            var client = new SmtpClient(appSettings.Value.SMTP, appSettings.Value.Port)
            {
                EnableSsl = true,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(appSettings.Value.EMail, appSettings.Value.Password)
                
            };
            var mailMessage = new MailMessage
            {
                From = new MailAddress(appSettings.Value.EMail, appSettings.Value.Organization)
            };
            mailMessage.To.Add(email);
            mailMessage.Subject = subject;
            mailMessage.Body = htmlMessage;
            return client.SendMailAsync(mailMessage);
        }
    }
}
