﻿using System;
using System.Collections.Generic;
using System.Text;
using BayTreasureChestAssociation.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace BayTreasureChestAssociation.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
        public DbSet<Player> Players { get; set; }
        public DbSet<Registration> Registrations { get; set; }
        public DbSet<Community> Communities { get; set; }
        public DbSet<CalendarDate> CalendarDates { get; set; }
        public DbSet<Draw> Draws { get; set; }
        public DbSet<DrawLineItemData> DrawLineItemDataSet { get; set; }
        public DbSet<WinnerLog> WinnerLogs { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<NonWinningDraw> NonWinningDraws { get; set; }
        public DbSet<ApplicationSettings> ApplicationSettings { get; set; }
        public DbSet<ApplicationUser> ApplicationUsers { get; set; }
    }
}
