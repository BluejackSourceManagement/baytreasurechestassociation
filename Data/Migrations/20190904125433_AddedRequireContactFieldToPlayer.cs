﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BayTreasureChestAssociation.Data.Migrations
{
    public partial class AddedRequireContactFieldToPlayer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "RequiresContact",
                table: "Players",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "RequiresContact",
                table: "Players");
        }
    }
}
