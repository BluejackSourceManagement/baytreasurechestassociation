﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BayTreasureChestAssociation.Data.Migrations
{
    public partial class AdditionalClassesModified : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "wasPlayed",
                table: "WinnerLogs",
                newName: "WasPlayed");

            migrationBuilder.RenameColumn(
                name: "hasRollover",
                table: "WinnerLogs",
                newName: "HasRollover");

            migrationBuilder.RenameColumn(
                name: "ID",
                table: "WinnerLogs",
                newName: "Id");

            migrationBuilder.RenameColumn(
                name: "hasWinner",
                table: "Draws",
                newName: "HasWinner");

            migrationBuilder.RenameColumn(
                name: "ID",
                table: "Draws",
                newName: "Id");

            migrationBuilder.RenameColumn(
                name: "ID",
                table: "DrawLineItemDataSet",
                newName: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "WasPlayed",
                table: "WinnerLogs",
                newName: "wasPlayed");

            migrationBuilder.RenameColumn(
                name: "HasRollover",
                table: "WinnerLogs",
                newName: "hasRollover");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "WinnerLogs",
                newName: "ID");

            migrationBuilder.RenameColumn(
                name: "HasWinner",
                table: "Draws",
                newName: "hasWinner");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "Draws",
                newName: "ID");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "DrawLineItemDataSet",
                newName: "ID");
        }
    }
}
