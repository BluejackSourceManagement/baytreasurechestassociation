﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BayTreasureChestAssociation.Data.Migrations
{
    public partial class CoalescedSeveralFieldsToAddressField : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Apartment",
                table: "Players");

            migrationBuilder.DropColumn(
                name: "StreetName",
                table: "Players");

            migrationBuilder.RenameColumn(
                name: "StreetNum",
                table: "Players",
                newName: "Address");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Address",
                table: "Players",
                newName: "StreetNum");

            migrationBuilder.AddColumn<string>(
                name: "Apartment",
                table: "Players",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "StreetName",
                table: "Players",
                nullable: true);
        }
    }
}
