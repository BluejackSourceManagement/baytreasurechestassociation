﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BayTreasureChestAssociation.Data.Migrations
{
    public partial class UpdateNonWinningDrawTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "DrawId",
                table: "NonWinningDraws",
                newName: "WeekNumber");

            migrationBuilder.AddColumn<DateTime>(
                name: "DrawDate",
                table: "NonWinningDraws",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DrawDate",
                table: "NonWinningDraws");

            migrationBuilder.RenameColumn(
                name: "WeekNumber",
                table: "NonWinningDraws",
                newName: "DrawId");
        }
    }
}
