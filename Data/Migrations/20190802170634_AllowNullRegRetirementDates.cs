﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BayTreasureChestAssociation.Data.Migrations
{
    public partial class AllowNullRegRetirementDates : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "DateRetired",
                table: "Registrations",
                nullable: true,
                oldClrType: typeof(DateTime));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "DateRetired",
                table: "Registrations",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldNullable: true);
        }
    }
}
