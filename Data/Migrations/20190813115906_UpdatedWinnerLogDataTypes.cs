﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BayTreasureChestAssociation.Data.Migrations
{
    public partial class UpdatedWinnerLogDataTypes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "RolloverAmount",
                table: "WinnerLogs",
                nullable: false,
                oldClrType: typeof(double));

            migrationBuilder.AlterColumn<decimal>(
                name: "PossiblePrize",
                table: "WinnerLogs",
                nullable: false,
                oldClrType: typeof(double));

            migrationBuilder.AlterColumn<decimal>(
                name: "BankDeposit",
                table: "WinnerLogs",
                nullable: false,
                oldClrType: typeof(double));

            migrationBuilder.AlterColumn<decimal>(
                name: "ActualPrize",
                table: "WinnerLogs",
                nullable: false,
                oldClrType: typeof(double));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<double>(
                name: "RolloverAmount",
                table: "WinnerLogs",
                nullable: false,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<double>(
                name: "PossiblePrize",
                table: "WinnerLogs",
                nullable: false,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<double>(
                name: "BankDeposit",
                table: "WinnerLogs",
                nullable: false,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<double>(
                name: "ActualPrize",
                table: "WinnerLogs",
                nullable: false,
                oldClrType: typeof(decimal));
        }
    }
}
