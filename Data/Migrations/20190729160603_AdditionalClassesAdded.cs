﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BayTreasureChestAssociation.Data.Migrations
{
    public partial class AdditionalClassesAdded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CalendarDates",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DateStart = table.Column<DateTime>(nullable: false),
                    DateEnd = table.Column<DateTime>(nullable: false),
                    WeekNumber = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CalendarDates", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Draws",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    hasWinner = table.Column<bool>(nullable: false),
                    Comments = table.Column<string>(nullable: true),
                    StickerCount = table.Column<int>(nullable: false),
                    UnknownStickerCount = table.Column<int>(nullable: false),
                    TotalStickerCount = table.Column<int>(nullable: false),
                    ExpectedStickerCount = table.Column<int>(nullable: false),
                    RegistrationID = table.Column<int>(nullable: true),
                    CalendarDateID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Draws", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Draws_CalendarDates_CalendarDateID",
                        column: x => x.CalendarDateID,
                        principalTable: "CalendarDates",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Draws_Registrations_RegistrationID",
                        column: x => x.RegistrationID,
                        principalTable: "Registrations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "DrawLineItemDataSet",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    SheetNumberStart = table.Column<int>(nullable: false),
                    SheetNumberEnd = table.Column<int>(nullable: false),
                    SheetNumberRange = table.Column<string>(nullable: true),
                    StickerCount = table.Column<int>(nullable: false),
                    UnknownStickerCount = table.Column<int>(nullable: false),
                    TotalStickerCount = table.Column<int>(nullable: false),
                    DrawID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DrawLineItemDataSet", x => x.ID);
                    table.ForeignKey(
                        name: "FK_DrawLineItemDataSet_Draws_DrawID",
                        column: x => x.DrawID,
                        principalTable: "Draws",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "WinnerLogs",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    wasPlayed = table.Column<bool>(nullable: false),
                    BankDeposit = table.Column<double>(nullable: false),
                    PossiblePrize = table.Column<double>(nullable: false),
                    ActualPrize = table.Column<double>(nullable: false),
                    LocationPlayed = table.Column<string>(nullable: true),
                    WinnersCommunity = table.Column<string>(nullable: true),
                    hasRollover = table.Column<bool>(nullable: false),
                    RolloverAmount = table.Column<double>(nullable: false),
                    RolloverCount = table.Column<int>(nullable: false),
                    DrawID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WinnerLogs", x => x.ID);
                    table.ForeignKey(
                        name: "FK_WinnerLogs_Draws_DrawID",
                        column: x => x.DrawID,
                        principalTable: "Draws",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DrawLineItemDataSet_DrawID",
                table: "DrawLineItemDataSet",
                column: "DrawID");

            migrationBuilder.CreateIndex(
                name: "IX_Draws_CalendarDateID",
                table: "Draws",
                column: "CalendarDateID");

            migrationBuilder.CreateIndex(
                name: "IX_Draws_RegistrationID",
                table: "Draws",
                column: "RegistrationID");

            migrationBuilder.CreateIndex(
                name: "IX_WinnerLogs_DrawID",
                table: "WinnerLogs",
                column: "DrawID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DrawLineItemDataSet");

            migrationBuilder.DropTable(
                name: "WinnerLogs");

            migrationBuilder.DropTable(
                name: "Draws");

            migrationBuilder.DropTable(
                name: "CalendarDates");
        }
    }
}
