﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BayTreasureChestAssociation.Data.Migrations
{
    public partial class UpdatedDrawAndWinnerLogSchemas : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "WinnersCommunity",
                table: "WinnerLogs",
                newName: "Comments");

            migrationBuilder.AddColumn<string>(
                name: "ChequeNumber",
                table: "WinnerLogs",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "BareCoins",
                table: "Draws",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "DisqualifiedPinkSheetStickers",
                table: "Draws",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "KnownVariance",
                table: "Draws",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<int>(
                name: "NewRegistrations",
                table: "Draws",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Supervisor",
                table: "Draws",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "UnderStickeredDollarBills",
                table: "Draws",
                nullable: false,
                defaultValue: 0m);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ChequeNumber",
                table: "WinnerLogs");

            migrationBuilder.DropColumn(
                name: "BareCoins",
                table: "Draws");

            migrationBuilder.DropColumn(
                name: "DisqualifiedPinkSheetStickers",
                table: "Draws");

            migrationBuilder.DropColumn(
                name: "KnownVariance",
                table: "Draws");

            migrationBuilder.DropColumn(
                name: "NewRegistrations",
                table: "Draws");

            migrationBuilder.DropColumn(
                name: "Supervisor",
                table: "Draws");

            migrationBuilder.DropColumn(
                name: "UnderStickeredDollarBills",
                table: "Draws");

            migrationBuilder.RenameColumn(
                name: "Comments",
                table: "WinnerLogs",
                newName: "WinnersCommunity");
        }
    }
}
