﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BayTreasureChestAssociation
{
    public class AppSettings
    {
        public string NSAGLicense { get; set; }
        public string EMail { get; set; }
        public string Password { get; set; }
        public string SMTP { get; set; }
        public int Port { get; set; }
        public string Organization { get; set; }
        public string DefaultAdminUserName { get; set; }
        public string DefaultAdminEmail { get; set; }
        public string DefaultAdminName { get; set; }
        public string DefaultAdminPhone { get; set; }
        public bool DefaultAdminEmailConfirmed { get; set; }
        public string DefaultAdminPassword { get; set; }

    }
}
